//
//  NotificationsService.swift
//  BlockchainApp
//
//  Created by Pavel Aristov on 30.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications

import Firebase
import FirebaseMessaging

import ApphudSDK


class NotificationsService: NSObject {
    
    private override init() { }
    
    static let shared = NotificationsService()
 
    func registerForPushNotifications(force: Bool = false, _ completion: ((Bool) -> Void)? = nil) {

        guard !force else {
            requestAuthorization(completion)
            return
        }
        
        checkAuthorizationStatus { isAuthorized in
            guard !isAuthorized else { return }
            
            self.requestAuthorization(completion)
        }
    }
    
    func unregisterForPushNotifications() {
        Global.isPushNotificationsEnabled = false
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    func removeAllPendingNotifications() {
        DispatchQueue.main.async {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        }
    }
    
    private func requestAuthorization(_ completion: ((Bool) -> Void)? = nil) {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            print("[NotificationsService] Permission granted: \(granted)")

            DispatchQueue.main.async {
                completion?(granted)
            }
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func checkAuthorizationStatus(completion: @escaping (Bool) -> Void) {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            DispatchQueue.main.async {
                completion(settings.authorizationStatus == .authorized)
            }
        }
    }
    
    func getNotificationSettings(completion: ((UNNotificationSettings) -> Void)? = nil) {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
//            print("[NotificationsService] Notification settings: \(settings)")
            
            DispatchQueue.main.async {
                
                completion?(settings)
                
                guard settings.authorizationStatus == .authorized else { return }
                
                Global.isPushNotificationsEnabled = true
                UIApplication.shared.registerForRemoteNotifications()
                if Global.needToAddDemoAlert {
                    
                    Global.needToAddDemoAlert = false
                    Global.planLocalNotification(identifier: "demoPeriodEndedNotification",
                                                   title: Localization.commonDemoPeriodExpiredPushTitle.localized,
                                                   body: Localization.commonDemoPeriodExpiredPushText.localized,
                                                   timeInterval: 60 * 60 * 24)
                }
            }
        }
    }
    
}

extension NotificationsService: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        Apphud.handlePushNotification(apsInfo: response.notification.request.content.userInfo)
        handleRecievedPushNotification(request: response.notification.request)
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        Apphud.handlePushNotification(apsInfo: notification.request.content.userInfo)
        completionHandler([]) // return empty array to skip showing notification banner
    }
    
    func handleRecievedPushNotification(request: UNNotificationRequest) {
        print(request.content.userInfo, request.identifier)
        
        if let _ = request.content.userInfo["rule_name"] {
            let vc = SubscribeDiscountController.storyboardViewController(in: UIStoryboard.storyboars.start)
            vc.type = .winback
            vc.modalPresentationStyle = .overFullScreen
            UIApplication.topMostViewController?.present(vc, animated: true, completion: nil)
        } else if request.identifier == "cancellationReason" {
            let vc = CancellationReasonController.storyboardViewController(in: UIStoryboard.storyboars.start)
            vc.modalPresentationStyle = .overFullScreen
            UIApplication.topMostViewController?.present(vc, animated: true, completion: nil)
        }
    }
}

extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Apphud.submitPushNotificationsToken(token: deviceToken, callback: nil)
        
        let tokenParts = deviceToken.map { data -> String in
          return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("[NotificationsService] Device Token: \(token)")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("[NotificationsService] Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("[NotificationsService] Remote instance ID token: \(result.token)")
                Global.pushToken = result.token
                API.SystemModule.sendPushToken()
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("[NotificationsService] Failed to register: \(error)")
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("[NotificationsService.MessagingDelegate] Registration Token: \(fcmToken)")
        Global.pushToken = fcmToken
        API.SystemModule.sendPushToken()
    }
}
