//
//  WCSessionManagerBlocks.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 03.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation
import WatchConnectivity

typealias ActivationDidComplete = (_ activationState: WCSessionActivationState, _ error: Error?) -> Void

class WCSessionManagerBlocks: NSObject {
    
    var activationDidComplete: ActivationDidComplete? = nil
    
    #if os(iOS)
    var didBecomeInactive: (() -> Void)? = nil
    var didDeactivate: (() -> Void)? = nil
    #endif
    
    var didReceiveMessage: (([String: Any]) -> Void)? = nil
    
    override init() {
        super.init()
        
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
    }
    
    private var session: WCSession {
        return .default
    }
    
    func sendMessage(_ message: [String: Any], replyHandler: (([String: Any]) -> Void)? = nil, errorHandler: ((Error) -> Void)? = nil) {
        session.sendMessage(message, replyHandler: replyHandler, errorHandler: errorHandler)
    }
}

extension WCSessionManagerBlocks: WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        activationDidComplete?(activationState, error)
    }
    
    #if os(iOS)
    func sessionDidBecomeInactive(_ session: WCSession) {
        didBecomeInactive?()
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        didDeactivate?()
    }
    #endif
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        didReceiveMessage?(message)
    }
}

extension WCSessionManagerBlocks {
    func transferUserInfo(_ userInfo: [String: Any]) {
        session.transferUserInfo(userInfo)
    }
    
    func updateApplicationContext(_ applicationContext: [String: Any], errorHandler: ((Error?) -> Void)? = nil) {
        do {
            try session.updateApplicationContext(applicationContext)
            errorHandler?(nil)
        } catch (let error) {
            print("[WCSessionManagerBlocks] Error in `updateApplicationContext(_:)`:", error.localizedDescription)
            errorHandler?(error)
        }
    }
}
