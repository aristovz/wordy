//
//  WCSessionManager.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 03.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation
import WatchConnectivity

protocol WCSessionManagerDelegate: class {
    func sessionManager(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?)
    func sessionManager(didBecomeInactive session: WCSession)
    func sessionManager(didDeactivate session: WCSession)
}

extension WCSessionManagerDelegate {
    func sessionManager(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) { }
    func sessionManager(didBecomeInactive session: WCSession) { }
    func sessionManager(didDeactivate session: WCSession) { }
}

class WCSessionManager: NSObject {
    
    private override init() {
        super.init()
        
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
    }
    
    static let shared = WCSessionManager()
    
    private var session: WCSession {
        return .default
    }
    
    private var delegates: [WCSessionManagerDelegate] = []
    
    func sendMessage(_ message: [String: Any], replyHandler: (([String: Any]) -> Void)? = nil, errorHandler: ((Error) -> Void)? = nil) {
        session.sendMessage(message, replyHandler: replyHandler, errorHandler: errorHandler)
    }
}

extension WCSessionManager {
    func addDelegate(_ delegate: WCSessionManagerDelegate) {
        
        guard delegates.firstIndex(where: { $0 === delegate }) == nil else {
            print("[WCSessionManager] Try to add contains delegate", delegate.self)
            return
        }
        
        delegates.append(delegate)
    }
    
    func removeDelegate(_ delegate: WCSessionManagerDelegate) {
        guard let delegateIndex = delegates.firstIndex(where: { $0 === delegate }) else {
            print("[WCSessionManager] needToRemoveDelegate could not be finded")
            return
        }
        
        delegates.remove(at: delegateIndex)
    }
}

extension WCSessionManager: WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        delegates.forEach {
            $0.sessionManager(session, activationDidCompleteWith: activationState, error: error)
        }
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        delegates.forEach {
            $0.sessionManager(didBecomeInactive: session)
        }
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        delegates.forEach {
            $0.sessionManager(didDeactivate: session)
        }
    }
}

extension WCSessionManager {
    func transferUserInfo(_ userInfo: [String: Any]) {
        session.transferUserInfo(userInfo)
    }
    
    func updateApplicationContext(_ applicationContext: [String: Any], errorHandler: ((Error?) -> Void)? = nil) {
        do {
            try session.updateApplicationContext(applicationContext)
            errorHandler?(nil)
        } catch (let error) {
            print("[WCSessionManager] Error in `updateApplicationContext(_:)`:", error.localizedDescription)
            errorHandler?(error)
        }
    }
}
