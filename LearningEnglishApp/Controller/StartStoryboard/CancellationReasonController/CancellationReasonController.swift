//
//  CancellationReasonController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 12.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class CancellationReasonController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction private func sendCancellationReasonAction(_ sender: UIButton) {
        API.SystemModule.sendEvent("cancel_subscription_\(sender.tag)")
        
        showAlert(text: Localization.cancellationReasonActionAlertText.localized, title: nil, action: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @IBAction private func closeButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
