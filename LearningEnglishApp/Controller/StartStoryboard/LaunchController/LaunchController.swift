//
//  LaunchController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 19.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class LaunchController: UIViewController {

    @IBOutlet private weak var continueButtonOutlet: UIButton!
    
    @IBOutlet private weak var studyingLanguageNameLabel: UILabel!
    @IBOutlet private weak var studyingLanguageFlagImageView: UIImageView!
    
    @IBOutlet private weak var inLanguageNameLabel: UILabel!
    @IBOutlet private weak var inLanguageFlagImageView: UIImageView!
    
    @IBOutlet private weak var translationExampleFirstLabel: UILabel!
    @IBOutlet private weak var originalExampleFirstLabel: UILabel!
    
    @IBOutlet private weak var translationExampleSecondLabel: UILabel!
    @IBOutlet private weak var originalExampleSecondLabel: UILabel!
    
    @IBOutlet private weak var examplesLabel: UILabel!
    @IBOutlet private weak var examplesStackView: UIStackView!
    @IBOutlet private weak var examplesSecondContainerView: UIView!
    
    @IBOutlet private var levelLabels: [UILabel]!
    @IBOutlet private var levelCheckIcons: [UIImageView]!
    
    private var gradients: [UIView : CALayer] = [:]
    
    private var studyingLanguage: Language? = nil
    private var inLanguage: Language? = nil
    
    private var selectedLevel: Complexity = .beginner {
        didSet { updateLevelUI() }
    }
    
    private var languagePairs: [LanguagePair] = []
    private var selectedPair: LanguagePair? {
        return languagePairs.first(where: { $0.to == studyingLanguage && $0.from == inLanguage })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        loadLanguagePairs()
        setupLevelUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateGradients(gradients: gradients)
    }
    
    private func setupUI() {
        gradients[continueButtonOutlet] = continueButtonOutlet.setButtonGradient()
        
        setupLevelUI()
    }
    
    private func loadLanguagePairs() {
        
        if let pair = languagePairs.first(where: { $0.id == Client.current?.settings.languageId }) {
            self.updateLanguageUI(pair)
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if SharedGlobal.isAuthorized {
            loadLanguagePairsRequest()
        } else {
            API.SystemModule.getToken(needLoadLanguages: false) { _, result in
                guard result else {
                    hud.hide(animated: true)
                    self.showWarningAlert(text: ("launch_error_alert_load_languages", "Произошла ошибка при попытке загрузки языков. Попробуйте позже или обратитесь в службу тех. поддержки"))
                    return
                }
                
                self.loadLanguagePairsRequest()
            }
        }
    }
    
    private func loadLanguagePairsRequest() {
        API.LanguageModule.getPairs { languagePairs in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard !languagePairs.isEmpty else {
                self.showWarningAlert(text: ("launch_error_alert_load_languages", "Произошла ошибка при попытке загрузки языков. Попробуйте позже или обратитесь в службу тех. поддержки"))
                return
            }
            
           self.languagePairs = languagePairs
            
            let currentClient = Client.current
            if let languageId = currentClient?.settings.languageId {
                if let pair = languagePairs.first(where: { $0.id == languageId }) {
                    self.updateLanguageUI(pair)
                } else {
                    currentClient?.settings.languageId = languagePairs.first?.id
                    currentClient?.updateSettings()
                    self.updateLanguageUI(languagePairs.first)
                }
            } else {
                if let pair = languagePairs.first {
                    currentClient?.settings.languageId = languagePairs.first?.id
                    currentClient?.updateSettings()
                    self.updateLanguageUI(pair)
                }
            }
        }
    }
    
    private func updateLanguageUI(_ pair: LanguagePair? = nil) {
    
        if let pair = pair {
            studyingLanguage = pair.to
            inLanguage = pair.from
        }
        
        if let language = studyingLanguage {
            studyingLanguageNameLabel.text = language.name
            studyingLanguageFlagImageView.sd_setImage(with: URL(string: language.flag), completed: nil)
        }
        
        if let language = inLanguage {
            inLanguageNameLabel.text = language.name
            inLanguageFlagImageView.sd_setImage(with: URL(string: language.flag), completed: nil)
        }
        
//        if let pair = selectedPair {
            
//            examplesLabel.isHidden = pair.samples.count < 1
//            examplesStackView.isHidden = pair.samples.count < 1
//            examplesSecondContainerView.isHidden = pair.samples.count < 2
            
//            if pair.samples.count > 0 {
                translationExampleFirstLabel.text = "To quit" //pair.samples[0].translationWord
                originalExampleFirstLabel.text = "бросать, покидать, оставлять" //pair.samples[0].originalWord
//            }
            
//            if pair.samples.count > 1 {
                translationExampleSecondLabel.text = "You should quit smoking" //pair.samples[1].translationWord
                originalExampleSecondLabel.text = "Ты должен бросить курить" //pair.samples[1].originalWord
//            }
//        }
    }
    
    private func updateLevelUI() {
        levelLabels.forEach({
            $0.font = UIFont.getAvenirFont(font: $0.tag == selectedLevel.level ? .medium : .roman, size: 18)
        })
        
        levelCheckIcons.forEach({
            $0.image = $0.tag == selectedLevel.level ? #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "unchecked")
        })
    }
    
    // MARK: - Setup UI when come from SettingsController
    private func setupLevelUI() {
        if let level = Client.current?.settings.complexity {
            selectedLevel = level
        }
    }
    
    
    private func getLanguages(_ studying: Bool) -> [Language] {
        
        var languages = [Language]()
        
        if studying {
            if let inLanguage = inLanguage {
                languages = languagePairs.filter({ $0.from == inLanguage }).map { $0.to }
            } else {
                languages = languagePairs.map { $0.to }
            }
        } else {
            if let studyingLanguage = studyingLanguage {
                languages = languagePairs.filter({ $0.to == studyingLanguage }).map { $0.from }
            } else {
                languages = languagePairs.map { $0.from }
            }
        }
        
        return languages.unique
    }
    
    @IBAction private func selectLanguageButtonsAction(_ sender: UIButton) {
        
        let vc = SelectLanguageController.storyboardViewController(in: UIStoryboard.storyboars.start)
        vc.languages = getLanguages(sender.tag == 0)
        vc.selectedLanguage = sender.tag == 0 ? studyingLanguage : inLanguage
        
        vc.didSelectLanguage = { language in
            if sender.tag == 0 {
                self.studyingLanguage = language
            } else {
                self.inLanguage = language
            }
            
            self.updateLanguageUI()
        }
        
        vc.modalPresentationStyle = .fullScreen
//        vc.modalTransitionStyle = .crossDissolve
        
        self.present(vc, animated: true)
    }
    
    @IBAction private func levelButtonsAction(_ sender: UIButton) {
        guard let level = Complexity.get(from: sender.tag) else { return }
        
        selectedLevel = level
    }
    
    @IBAction private func continueButtonAction(_ sender: UIButton) {
        
        let client = Client.current
        client?.settings.complexity = selectedLevel
        client?.settings.languageId = selectedPair?.id
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        client?.updateSettings({ result in
            hud.hide(animated: true)
            
            guard result else {
                self.showWarningAlert(text: ("common_error_alert_save_settings", "Произошла ошибка при попытке сохранить данные. Попробуйте позже или обратитесь в службу тех. поддержки"))
                return
            }
            
            let vc = TopicsController.storyboardViewController(in: UIStoryboard.storyboars.start)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true)
        })
    }
}
