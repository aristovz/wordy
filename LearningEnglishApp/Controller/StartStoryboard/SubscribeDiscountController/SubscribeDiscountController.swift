//
//  SubscribeDiscountController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import ApphudSDK
import MBProgressHUD

enum SubscribeDiscountControllerType {
    case daily, monthly, winback
}

class SubscribeDiscountController: UIViewController {

    @IBOutlet private weak var subscribeButtonOutlet: UIButton!
    @IBOutlet private weak var subscribeInformationContainerView: UIView!
    
    @IBOutlet private var underlineButtons: [UIButton]!
    
    // Set auto size font fot buttons
    @IBOutlet private var autoFontSizeButtons: [UIButton]!
    
    private var gradients: [UIView : CALayer] = [:]
    
    var type: SubscribeDiscountControllerType = .daily
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateGradients(gradients: gradients)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateGradients(gradients: gradients)
        subscribeInformationContainerView.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50)
    }
    
    private func setupUI() {
        gradients[subscribeButtonOutlet] = subscribeButtonOutlet.setButtonGradient()
        
        underlineButtons.forEach {
            $0.underline()
        }
        
        autoFontSizeButtons.forEach {
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    
    private func showMainViewController() {
        guard let vc = UIStoryboard.storyboars.main.instantiateInitialViewController() else { return }
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        
        Global.isStartStoryboardCompleted = true
        
        self.present(vc, animated: true)
    }
    
    @IBAction private func subscribeButtonAction(_ sender: UIButton) {
        #if targetEnvironment(simulator)
        self.showMainViewController()
        return
        #else
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        Apphud.purchaseProduct(product: .yearlyWithSale) { subscription, error in
            
            hud.hide(animated: true)
            
            guard error == nil else {
                self.showAlert(text: error ?? "Undefind Error")
                return
            }
            
            if let _ = subscription {
                Global.monthlyDemoPeriodEnded = nil
                NotificationsService.shared.removeAllPendingNotifications()
                self.showMainViewController()
            } else {
                self.showWarningAlert(text: (key: "common_product_not_found", original: "Продукт не найден"))
            }
        }
        #endif
    }
    
    @IBAction private func closeButtonAction(_ sender: UIButton) {
        
        if type == .monthly {
            Global.startMonthlyDemoPeriod()
            dismiss(animated: true, completion: nil)
            return
        } else {
            
            if type == .winback {
                
                var timeInterval = 60 * 5
                
                #if DEBUG
                timeInterval = 15
                #endif
                
                Global.planLocalNotification(identifier: "cancellationReason", title: Localization.discountCancelReasonTitle.localized, body: Localization.discountCancelReasonMessage.localized, timeInterval: TimeInterval(timeInterval))
            }
            
            let vc = SubscriptionController.storyboardViewController(in: UIStoryboard.storyboars.start)
            vc.hideDemoButton = true
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true)
    //        dismiss(animated: true)
        }
    }
    
    @IBAction private func restorePurchasesButtonAction(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        Apphud.restorePurchases { subscriptions, _, error  in
            hud.hide(animated: true)
            
            if Apphud.hasActiveSubscriptionForever() {
                self.showMainViewController()
            } else {
                self.showWarningAlert(text: (key: "common_restore_failed", original: "Покупки не удалось восстановить"))
            }
        }
    }
    
    @IBAction private func termsOfUseButtonAction(_ sender: UIButton) {
        openURL(string: Global.termsOfUseLink)
    }
    
    @IBAction private func privacyPolicyButtonAction(_ sender: UIButton) {
        openURL(string: Global.privacyPolicyLink)
    }
}
