//
//  OnboardingController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 05.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class OnboardingController: UIViewController {

    @IBOutlet private var pageViews: [UIView]!
    
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var pageControlBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var allowPushNotificationsButtonOutlet: UIButton!
    
    private var currentPage = 0
    
    private var isPushNotificationsDenied = false
    
    private var allowNotificationsPage: UIView? {
        return pageViews.first(where: { $0.tag == 5 })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func setupUI() {
        NotificationsService.shared.getNotificationSettings { settings in
            
            guard settings.authorizationStatus != .authorized else {
                self.pageControl.numberOfPages = 5
                self.allowNotificationsPage?.isHidden = true
                self.allowPushNotificationsButtonOutlet.setTitle(Localization.onboardingButtonContinue.localized, for: .normal)
                return
            }
            
            self.isPushNotificationsDenied = settings.authorizationStatus == .denied
            self.allowPushNotificationsButtonOutlet.setTitle(settings.authorizationStatus == .denied ? Localization.onboardingButtonSettings.localized : Localization.onboardingButtonAllow.localized, for: .normal)
        }
    }

    private func setupButtonsUI() {
        let isLastPage = currentPage == pageControl.numberOfPages - 1
        
        pageControlBottomConstraint.constant = isLastPage ? 100 : 25
        UIView.animate(withDuration: 0.2) {
            self.allowPushNotificationsButtonOutlet.alpha = isLastPage ? 1 : 0
            self.pageControl.layoutIfNeeded()
        }
    }
    
    @IBAction private func allowPushNotificationsButtonAction(_ sender: UIButton) {
        
        if isPushNotificationsDenied {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString),
                    UIApplication.shared.canOpenURL(settingsUrl) else {
                print("Can't open settings url")
                return
            }
            
            UIApplication.shared.open(settingsUrl)
        } else {
            NotificationsService.shared.registerForPushNotifications(force: true) { result in
                
                guard !Global.isOnboardingCompleted else {
                    self.dismiss(animated: true)
                    return
                }
                
                guard let vc = UIStoryboard.storyboars.start.instantiateInitialViewController() else { return }
                vc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .crossDissolve
                Global.isOnboardingCompleted = true
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension OnboardingController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        currentPage = pageControl.currentPage
        
        setupButtonsUI()
    }
}
