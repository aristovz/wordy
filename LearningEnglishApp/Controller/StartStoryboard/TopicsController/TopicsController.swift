//
//  TopicsController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 19.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import ApphudSDK
import MBProgressHUD

class TopicsController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!
    
    @IBOutlet private weak var continueButtonOutlet: UIButton!
    
    var didSelectCategories: (([Category]) -> Void)? = nil
    
    private var categories: [[Category]] = [] {
        didSet {
            collectionView.reloadData()
            
            let startSelectedCategories = Client.current?.settings.categories ?? []
            for category in startSelectedCategories {
            
                var indexPath: IndexPath? = nil
                if let recomendedIndex = categories.first?.firstIndex(of: category) {
                    indexPath = IndexPath(row: recomendedIndex, section: 0)
                }
                
                if let simpleIndex = categories.last?.firstIndex(of: category) {
                    indexPath = IndexPath(row: simpleIndex, section: 1)
                }
                
                guard let _indexPath = indexPath else { return }
                
                collectionView.selectItem(at: _indexPath, animated: false, scrollPosition: .top)
            }
            
            collectionView.setContentOffset(.zero, animated: false)
            updateCountUI()
        }
    }
    private var selectedCategories: [Category] {
        return collectionView.indexPathsForSelectedItems?.map { categories[$0.section][$0.row] } ?? []
    }
    
    private var gradients: [UIView : CALayer] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupCollectionView()
        
        loadCategories()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateGradients(gradients: gradients)
    }
    
    private func setupUI() {
        gradients[continueButtonOutlet] = continueButtonOutlet.setButtonGradient()
    }
    
    private func setupCollectionView() {
        
        collectionView.contentInset.bottom = 20
        
        collectionView.allowsMultipleSelection = true
        
        collectionView.register(TopicCell.self)
        collectionView.register(TopicCollectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
        collectionView.register(TopicSectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
    }
    
    private func loadCategories() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        API.LanguageModule.getCategories { categories in
            hud.hide(animated: true)
            guard !categories.isEmpty else {
                self.showWarningAlert(text: ("common_error_alert_load_categories", "Произошла ошибка при попытке загрузки категорий. Попробуйте позже или обратитесь в службу тех. поддержки")) {
                    self.dismiss(animated: true, completion: nil)
                }
                return
            }
            
            var recomended: [Category] = []
            var simple: [Category] = []
            
            categories.forEach { category in
                if category.isRecommended {
                    recomended.append(category)
                } else {
                    simple.append(category)
                }
            }
            
            self.categories = [recomended, simple]
        }
    }
    
    private func updateCountUI() {
        let selectedItemsCount = collectionView.indexPathsForSelectedItems?.count ?? 0
        
        let goOnString = Localization.selectCategoriesGoOn.getLzText(original: "Go on")
        continueButtonOutlet.setTitle(goOnString + "\(selectedItemsCount == 0 ? "" : " (\(selectedItemsCount))")", for: .normal)
    }
    
    private func showSubscriptionController() {
        let vc = SubscriptionController.storyboardViewController(in: UIStoryboard.storyboars.start)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true)
    }
    
    private func showMainController() {
        guard let vc = UIStoryboard.storyboars.main.instantiateInitialViewController() else { return }
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        
        Global.isStartStoryboardCompleted = true
        
        self.present(vc, animated: true)
    }
    
    @IBAction private func continueButtonAction(_ sender: UIButton) {
        
        guard !selectedCategories.isEmpty else {
            showWarningAlert(text: ("select_categories_alert_not_selected", "Необходимо выбрать хотя бы одну категорию"))
            return
        }
        
        if let didSelectCategories = didSelectCategories {
            didSelectCategories(selectedCategories)
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        let client = Client.current
        client?.settings.categories = selectedCategories
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        client?.updateSettings({ result in
            hud.hide(animated: true)
            
            guard result else {
                self.showWarningAlert(text: ("common_error_alert_save_settings", "Произошла ошибка при попытке сохранить данные. Попробуйте позже или обратитесь в службу тех. поддержки"))
                return
            }
            
//            if Apphud.hasActiveSubscriptionForever() {
//                self.showMainController()
//            } else {
                self.showSubscriptionController()
//            }
        })
    }
}

extension TopicsController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return categories.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories[section].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as TopicCell
        cell.setupUI(categories[indexPath.section][indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        updateCountUI()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        updateCountUI()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            
            if indexPath.section == 0 {
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, forIndexPath: indexPath) as TopicCollectionHeader
                
                view.set(title: Localization.selectCategoriesRecomended.getLzText(original: "Recomended"))
                
                return view
            } else {
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, forIndexPath: indexPath) as TopicSectionHeader
                
                switch indexPath.section {
                case 1: view.set(title: Localization.selectCategoriesAllCategories.getLzText(original: "All Categories"))
                default: ()
                }
                
                return view
            }
        default:
            assert(false, "Invalid element type")
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: section == 0 ? (UIScreen.main.bounds.size.height < 650 ? 133 : 153) : 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        
        let itemSpacing = flowayout?.minimumInteritemSpacing ?? 0.0
        let leftInset = flowayout?.sectionInset.left ?? 0.0
        let rightInset = flowayout?.sectionInset.right ?? 0.0
        
        let space: CGFloat = itemSpacing + leftInset + rightInset
        let size: CGFloat = (collectionView.frame.size.width - space) / 2.0
        
        return CGSize(width: size, height: size)
    }
}
