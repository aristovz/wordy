//
//  TopicCell.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit

class TopicCell: UICollectionViewCell {

    @IBOutlet private weak var containerView: UIView!
    
    @IBOutlet private weak var colorIndicatorView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    @IBOutlet private weak var selectedIcon: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            containerView.layer.borderWidth = isSelected ? 1 : 0
            selectedIcon.isHidden = !isSelected
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.borderColor = Pallete.purple.cgColor
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        containerView.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50)
    }
    
    func setupUI(_ category: Category) {
        titleLabel.text = category.name
        
        
        colorIndicatorView.backgroundColor = category.color
        
        if category.name.contains(" ") {
            titleLabel.setLineSpacing(lineHeightMultiple: 1.3)
        }
        
        titleLabel.numberOfLines = category.name.contains(" ") ? 2 : 1
    }
}

extension UILabel {
    func adjustFontSizeToFit(minimumFontSize: CGFloat, maximumFontSize: CGFloat? = nil) {
        let maxFontSize = maximumFontSize ?? font.pointSize
        for size in stride(from: maxFontSize, to: minimumFontSize, by: -CGFloat(0.1)) {
            let proposedFont = font.withSize(size)
            let constraintSize = CGSize(width: bounds.size.width, height: CGFloat(MAXFLOAT))
            let labelSize = ((text ?? "") as NSString).boundingRect(with: constraintSize,
                                                                            options: .usesLineFragmentOrigin,
                                                                            attributes: [NSAttributedString.Key.font: proposedFont],
                                                                            context: nil)
            if labelSize.height <= bounds.size.height {
                font = proposedFont
                setNeedsLayout()
                break;
            }
        }
    }
}
