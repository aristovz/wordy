//
//  TopicCollectionHeader.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit

class TopicCollectionHeader: UICollectionReusableView {

    @IBOutlet private weak var titleLabel: UILabel!
    
    static let reuseIdentifier = "TopicCollectionHeader"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(title: String) {
        titleLabel.text = title
    }
}
