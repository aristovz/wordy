//
//  LanguageCell.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 19.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import SDWebImage

class LanguageCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var flagImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        flagImageView.layer.borderColor = Pallete.LanguageCell.purple.cgColor
    }
    
    func setupUI(_ language: Language, isSelected: Bool = false) {
        nameLabel.text = language.name
        flagImageView.sd_setImage(with: URL(string: language.flag), completed: nil)
        
        flagImageView.layer.borderWidth = isSelected ? 2 : 0
        nameLabel.font = UIFont.getAvenirFont(font: isSelected ? .medium : .roman, size: 14)
        nameLabel.textColor = isSelected ? Pallete.LanguageCell.purple : Pallete.LanguageCell.gray
    }
}
