//
//  SelectLanguageController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 19.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit

class SelectLanguageController: UIViewController {

    @IBOutlet private weak var searchTextFieldContainerView: UIView!
    @IBOutlet private weak var searchTextField: UITextField!
    
    @IBOutlet private weak var tableView: UITableView!
    
    var languages: [Language] = []
    var selectedLanguage: Language? = nil
    
    var didSelectLanguage: ((Language) -> Void)? = nil
    
    private var filteredLanguages: [Language] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filteredLanguages = languages
        
        setupTableView()
        hideKeyboardWhenTappedAround(cancelsTouchesInView: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        searchTextFieldContainerView.dropShadow(color: Pallete.SelectLanguageController.shadow, opacity: 0.07, offSet: CGSize(width: 0, height: 1), radius: 0)
    }
    
    private func setupTableView() {
        tableView.register(LanguageCell.self)
        
        tableView.contentInset.top = 15
        tableView.scrollIndicatorInsets.top = 15
    }
    
    @IBAction private func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

extension SelectLanguageController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredLanguages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as LanguageCell
        
        let language = filteredLanguages[indexPath.row]
        cell.setupUI(language, isSelected: language == selectedLanguage)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var updatedIndexPaths = [indexPath]
        if let language = selectedLanguage,
            let oldIndex = filteredLanguages.firstIndex(of: language) {
            updatedIndexPaths.append(IndexPath(row: oldIndex, section: 0))
        }
        
        selectedLanguage = filteredLanguages[indexPath.row]
        
        tableView.reloadRows(at: updatedIndexPaths, with: .automatic)
        
        didSelectLanguage?(filteredLanguages[indexPath.row])
        dismiss(animated: true)
    }
}

extension SelectLanguageController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        dismissKeyboard()
    }
}

extension SelectLanguageController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)

        guard let query = updatedString, !query.isEmpty else {
            filteredLanguages = languages
            return true
        }
        
        filteredLanguages = languages.filter { $0.name.lowercased().contains(query.lowercased()) }
        
        return true
    }
    
}
