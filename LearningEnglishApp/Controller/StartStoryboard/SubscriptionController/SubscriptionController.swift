//
//  SubscriptionController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import ApphudSDK
import MBProgressHUD

class SubscriptionController: UIViewController {

    @IBOutlet private weak var tryDemoButtonOutlet: UIButton!
    
    @IBOutlet private var planViews: [UIView]!
    @IBOutlet private var planSelectedIcons: [UIImageView]!
    
    @IBOutlet private weak var subscribeButtonOutlet: UIButton!
    @IBOutlet private weak var subscribeInformationContainerView: UIView!
    
    @IBOutlet private weak var closeButtonOutlet: UIButton!
    
    @IBOutlet private var underlineButtons: [UIButton]!
    
    // Set auto size font fot buttons
    @IBOutlet private var autoFontSizeButtons: [UIButton]!
    
    private var gradients: [UIView : CALayer] = [:]
    
    private var selectedPlanIndex = 0 {
        didSet {
            updateSelectedPlanUI()
        }
    }
    private var selectedProduct: Apphud.Product {
        switch selectedPlanIndex {
        case 0: return .yearly
        case 1: return .monthly
        case 2: return .yearlyWithTrial
        default:
            print("Undefind planIndex")
            return .yearly
        }
    }
    
    private var isPurchaseStarted: Bool = false
    
    var hideDemoButton = false
    
    deinit {
      NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global.monthlyDemoPeriodEnded = nil
        
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateGradients(gradients: gradients)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateGradients(gradients: gradients)
        
        planViews.forEach { $0.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50) }
        subscribeInformationContainerView.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50)
    }
    
    private func setupUI() {
        updateSelectedPlanUI()
        tryDemoButtonOutlet.underline()
        tryDemoButtonOutlet.isHidden = hideDemoButton
        
        closeButtonOutlet.isHidden = hideDemoButton
        
        planViews.forEach({ $0.layer.borderColor = Pallete.purple.cgColor })
        gradients[subscribeButtonOutlet] = subscribeButtonOutlet.setButtonGradient()
        
        underlineButtons.forEach {
            $0.underline()
        }
        
        autoFontSizeButtons.forEach {
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        
        if Apphud.products() == nil {
            NotificationCenter.default.addObserver(self, selector: #selector(productDidLoad), name: Apphud.didFetchProductsNotification(), object: nil)
        }
    }
    
    private func updateSelectedPlanUI() {
        planViews.forEach({ $0.layer.borderWidth = $0.tag == selectedPlanIndex ? 1 : 0 })
        planSelectedIcons.forEach({ $0.isHidden = $0.tag != selectedPlanIndex })
    }
    
    @objc private func productDidLoad() {
        MBProgressHUD.hide(for: self.view, animated: true)
        if isPurchaseStarted {
            subscribeButtonAction(subscribeButtonOutlet)
        }
    }
    
    private func showMainViewController() {
        guard let vc = UIStoryboard.storyboars.main.instantiateInitialViewController() else { return }
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        
        Global.isStartStoryboardCompleted = true
        
        self.present(vc, animated: true)
    }
    
    @IBAction private func selectPlanButtonsAction(_ sender: UIButton) {
        selectedPlanIndex = sender.tag
    }
    
    @IBAction private func tryDemoButtonAction(_ sender: UIButton) {
        Global.startDemoPeriod()
        self.showMainViewController()
    }
    
    @IBAction private func subscribeButtonAction(_ sender: UIButton) {
        
        isPurchaseStarted = true
        print(Apphud.products() != nil ? "NOT EMPTY" : "EMPTY")
        
        guard Apphud.products() != nil else { return }
        
        #if targetEnvironment(simulator)
        if selectedPlanIndex == 1 {
            Global.startMonthlyDemoPeriod()
        }
        self.showMainViewController()
        return
        #else
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        Apphud.purchaseProduct(product: selectedProduct) { subscription, error in
            self.isPurchaseStarted = false
            hud.hide(animated: true)
            
            guard error == nil else {
                self.showAlert(text: error ?? "Undefind Error")
                return
            }
            
            if let _ = subscription {
                
                Global.monthlyDemoPeriodEnded = nil
                NotificationsService.shared.removeAllPendingNotifications()
                
                if self.selectedProduct == .monthly {
                    Global.startMonthlyDemoPeriod()
                }
                
                self.showMainViewController()
            } else {
                self.showWarningAlert(text: (key: "common_product_not_found", original: "Продукт не найден"))
            }
        }
        #endif
    }
    
    @IBAction private func restorePurchasesButtonAction(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        Apphud.restorePurchases { subscriptions, _, _ in
            hud.hide(animated: true)
            
            if Apphud.hasActiveSubscriptionForever() {
                self.showMainViewController()
            } else {
                self.showWarningAlert(text: (key: "common_restore_failed", original: "Покупки не удалось восстановить"))
            }
        }
    }
    
    @IBAction private func termsOfUseButtonAction(_ sender: UIButton) {
        openURL(string: Global.termsOfUseLink)
    }
    
    @IBAction private func privacyPolicyButtonAction(_ sender: UIButton) {
        openURL(string: Global.privacyPolicyLink)
    }
    
    @IBAction private func closeButtonAction(_ sender: UIButton) {
        
        guard !hideDemoButton else { return }
        
        Global.startDemoPeriod()
        self.showMainViewController()
    }
}
