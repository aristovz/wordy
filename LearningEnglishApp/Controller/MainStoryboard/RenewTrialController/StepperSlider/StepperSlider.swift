//
//  StepperSlider.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 08.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class StepperSlider: UISlider {
    
    public var selectedIndexChanged: ((Int) -> Void)? = nil
    public var valueChanged: ((Float) -> Void)? = nil
    
    public var selectedIndex: Int = 1 {
        didSet {
            guard oldValue != selectedIndex else { return }
            
            selectedIndexChanged?(selectedIndex)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpSlider()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpSlider()
    }

    private func setUpSlider() {
        self.setThumbImage(#imageLiteral(resourceName: "sliderThumbImage"), for: .normal)
        addTarget(self, action: #selector(handleValueChange(sender:event:)), for: .valueChanged)
    }

    @objc func handleValueChange(sender: UISlider, event: UIEvent) {
        
        let newValue = sender.value.rounded()
        
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .moved:
                valueChanged?(sender.value)
                selectedIndex = Int(newValue)
            case .ended:
                setValue(newValue, animated: false)
            default: break
            }
        }
    }
}
