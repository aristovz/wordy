//
//  TrainingController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import MBProgressHUD

class TrainingController: UIViewController {

    @IBOutlet private weak var originalWordLabel: UILabel!
    
    @IBOutlet private weak var learnButtonOutlet: UIButton!
    @IBOutlet private weak var learnButtonTrailingConstraing: NSLayoutConstraint!
    
    @IBOutlet private var wordViews: [UIView]!
    @IBOutlet private var answerButtons: [UIButton]!
    
    @IBOutlet private weak var upperStackView: UIStackView!
    @IBOutlet private weak var bottomStackView: UIStackView!
    
    @IBOutlet private var wordsRightIcons: [UIImageView]!
    @IBOutlet private var wordsWrongIcons: [UIImageView]!
    
    @IBOutlet private weak var shadowWordView: UIView!
    
    @IBOutlet private weak var nextTrainingButtonOutlet: UIButton!
    
    // Set auto size font fot buttons
    @IBOutlet private var autoFontSizeButtons: [UIButton]!
    
    private var training: Training! {
        didSet {
            updateTrainingUI()
        }
    }
    
    private var gradients: [UIView : CALayer] = [:]
    
    private var selectedWordIndex = -1 {
        didSet {
            updateSelectedWordUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if training == nil {
            loadNewTraining()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateGradients(gradients: gradients)
        
        wordViews.forEach { $0.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50) }
        shadowWordView.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50)
    }
    
    private func setupUI() {
        gradients[learnButtonOutlet] = learnButtonOutlet.setButtonGradient()
        
        originalWordLabel.text = "---"
//        learnButtonOutlet.alpha = 0
        upperStackView.isHidden = true
        bottomStackView.isHidden = true
        
        answerButtons.forEach {
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        
        autoFontSizeButtons.forEach {
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    
    private func updateSelectedWordUI() {
        
        guard training != nil else { return }
        guard let answer = training.getAnswer(at: selectedWordIndex) else { return }
        
        wordViews.forEach({ $0.layer.borderColor = Pallete.TrainingController.purple.cgColor })
        wordViews.forEach({ $0.layer.borderWidth = $0.tag == selectedWordIndex ? 1 : 0 })
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        API.TrainingModule.check(originalWord: training.originalWord, answer: answer) { rightAnswer in
            hud.hide(animated: true)
            
            let result = rightAnswer == nil
            
            if let rightAnswer = rightAnswer {
                self.wordViews.forEach({ $0.layer.borderColor = Pallete.TrainingController.red.cgColor })
                self.wordsWrongIcons.forEach({ $0.isHidden = $0.tag != self.selectedWordIndex })
                
                if let rightWordIndex = self.training.answers.firstIndex(of: rightAnswer) {
                    self.wordsRightIcons.forEach({ $0.isHidden = $0.tag != rightWordIndex })
                }
            } else {
                self.wordsRightIcons.forEach({ $0.isHidden = $0.tag != self.selectedWordIndex })
            }
            
            let correctAnswer = ("training_alert_result_answer_correct", "Вы дали правильный ответ!")
            let wrongAnswer = ("training_alert_result_answer_wrong", "Вы дали неправильный ответ!")
            
            self.answerButtons.forEach { $0.isEnabled = false }
            self.showWarningAlert(text: result ? correctAnswer : wrongAnswer) {
                self.nextTrainingButtonOutlet.show()
            }
        }
    }
    
    private func updateTrainingUI() {
        
        guard training != nil else { return }
        
        nextTrainingButtonOutlet.hide()
        showHideLearningButton(isNeedShow: true)
                
        originalWordLabel.text = training.originalWord
        originalWordLabel.numberOfLines = training.originalWord.contains(" ") ? 2 : 1
        
        answerButtons.forEach {
            let answer = training.getAnswer(at: $0.tag)
            $0.superview?.isHidden = answer == nil
            $0.setTitle(answer, for: .normal)
            $0.isEnabled = true
        }
        
        upperStackView.isHidden = false
        bottomStackView.isHidden = training.answers.count < 3
        
        wordViews.forEach({ $0.layer.borderWidth = 0 })
        wordsRightIcons.forEach({ $0.isHidden = true })
        wordsWrongIcons.forEach({ $0.isHidden = true })
    }
    
    private func showHideLearningButton(isNeedShow: Bool) {
        if isNeedShow {
            learnButtonTrailingConstraing.constant = 17
            learnButtonOutlet.layoutIfNeeded()
            learnButtonOutlet.show()
        } else {
            learnButtonOutlet.hide {
                self.learnButtonTrailingConstraing.constant = -107
                self.learnButtonOutlet.layoutIfNeeded()
            }
        }
    }
    
    private func loadNewTraining() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        API.TrainingModule.getNew { training in
            hud.hide(animated: true)
            guard let training = training else {
                self.showWarningAlert(text: ("training_alert_empty_words", "Доступные тренировки закончились. Попробуйте позже"))
                self.nextTrainingButtonOutlet.show()
                self.training = nil
                return
            }
            
            self.training = training
        }
    }
    
    @IBAction private func selectWordButtonsAction(_ sender: UIButton) {
        selectedWordIndex = sender.tag
    }
    
    @IBAction private func learnButtonAction(_ sender: UIButton) {
        
        guard training != nil else { return }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        API.TrainingModule.saveToLearn(originalWord: training.originalWord) { result in
            hud.hide(animated: true)
            guard result else {
                self.showWarningAlert(text: ("training_error_alert_learn", "Произола ошибка при попытке сохранить слово для изучения. Попробуйте позже или обратитесь в службу тех. поддержки"))
                return
            }
            
            self.showHideLearningButton(isNeedShow: false)
        }
        
    }
    
    @IBAction private func nextTrainingButtonAction(_ sender: UIButton) {
        loadNewTraining()
    }
}
