//
//  WordsController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import ApphudSDK
import MBProgressHUD

import Reachability

class WordsController: UIViewController {

    @IBOutlet private weak var shadowTimeView: UIView!
    @IBOutlet private weak var wordsContainerShadowView: UIView!
    @IBOutlet private weak var historyContainerShadowView: UIView!
    
    @IBOutlet private weak var hoursCountLabel: UILabel!
    @IBOutlet private weak var minutesCountLabel: UILabel!
    
    @IBOutlet private weak var hoursLabel: UILabel!
    @IBOutlet private weak var minutesLabel: UILabel!
    
    @IBOutlet private weak var currentWordContainerView: UIView!
    @IBOutlet private weak var currentWordView: ClientWordView!
    
    @IBOutlet private weak var historyContainerView: UIView!
    @IBOutlet private weak var historyStackView: UIStackView!
    @IBOutlet private weak var historyPageControl: UIPageControl!
    
    @IBOutlet private weak var knowThisWordButtonOutlet: UIButton!
    @IBOutlet private weak var shareButtonOutlet: UIButton!
    
    private var needUpdateCurrentWord = false
    
    private let reachability = try! Reachability()
    
    private var currentWord: ClientWord! {
        didSet {
            guard currentWord != nil else { return }
            
            watchKitSessionManager.sendMessage(["newCurrentWord": currentWord!.map])
            
            if currentWord == oldValue {
                self.updateCurrentWord()
            } else {
                needUpdateCurrentWord = false
            }
            
//            guard currentWord != oldValue else {
//                print("Update only time")
//                setTime()
//                return
//            }
            
            updateCurrentWordUI()
        }
    }
    
    private var history: [ClientWord] = [] {
        didSet {
            
            watchKitSessionManager.sendMessage(["newHistory": history.map({ $0.map })])
            
            updateHistoryUI()
        }
    }
    
    private var isTimerUpdate = false
    private var timer: Timer!
    private var time: (hours: Int, minutes: Int, seconds: Int) = (1, 24, 0) {
        didSet {
            updateTimeUI()
        }
    }
    
    private var watchKitSessionManager = WCSessionManagerBlocks()
    
    // Set auto size font fot buttons
    @IBOutlet private var autoFontSizeButtons: [UIButton]!
    
    private var gradients: [UIView : CALayer] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        NotificationsService.shared.registerForPushNotifications(force: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshData(needUpdateHistory: currentWord == nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkSubscription()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateGradients(gradients: gradients)
        
        shadowTimeView.setDefaultShadow()
        wordsContainerShadowView.setDefaultShadow()
        historyContainerShadowView.setDefaultShadow()
    }
    
    private func setupUI() {
        shareButtonOutlet.layer.borderWidth = 1
        shareButtonOutlet.layer.borderColor = Pallete.WordsController.shareBorder.cgColor
        
        gradients[knowThisWordButtonOutlet] = knowThisWordButtonOutlet.setButtonGradient()
        
        currentWordView.bottomContentInset = 90
        
        currentWordContainerView.alpha = 0
        currentWordContainerView.isHidden = true
        
        historyContainerView.alpha = 0
        historyContainerView.isHidden = true
        
        autoFontSizeButtons.forEach {
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        
        setupWatchKitSessionManager()
        
        reachability.whenReachable = { reachability in
            self.refreshData(needUpdateHistory: self.currentWord == nil)
        }

        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
//        updateTimeUI()
    }
    
    private func setupWatchKitSessionManager() {
        
        watchKitSessionManager.activationDidComplete = { state, error in
            print(state.rawValue)
        }
        
    }
    
    private func startTimer() {
        
        if timer != nil {
            timer.invalidate()
        }
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] _ in
            
            guard let self = self else { return }
            guard self.currentWord != nil else { return }
            
//            print("Time to update", self.time, self.currentWord.timeToNextWord)
            
            self.time = self.currentWord.timeToNextWord
            self.currentWordView.setupUI(self.currentWord)
            
            if self.time.hours <= 0 && self.time.minutes <= 1 && self.time.seconds <= 0 {
                self.timer.invalidate()
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    print("Update data by timer")
                    self.needUpdateCurrentWord = true
                    self.isTimerUpdate = true
                    self.refreshData()
                }
                
//                self.knowThisWordButtonAction(self.knowThisWordButtonOutlet)
            }
        })
    }
    
    private func updateCurrentWord() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            guard self.needUpdateCurrentWord else { return }
            
            self.refreshData()
        }
    }
    
    private func refreshData(needUpdateHistory: Bool = true) {
        
        print("Refresh data")
        
        guard reachability.connection != .unavailable else {
            self.showAlert(text: Localization.commonInternetConnectionProblem.localized)
            return
        }
        
        if needUpdateHistory {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        API.WordsModule.getToLearn { clientWord in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard let clientWord = clientWord else {
                self.showEmptyWordsAllert()
                self.updateCurrentWord()
                return
            }
            
            self.currentWord = clientWord
        }
     
        if needUpdateHistory {
            refreshHistory()
        }
    }
    
    private func refreshHistory(completion: (() -> (Void))? = nil) {
        API.WordsModule.getHistory { history in
            self.history = history
            completion?()
        }
    }
    
    private func updateTimeUI() {
        
        let hours = time.hours
        let minutes = time.minutes
        
        hoursCountLabel.text = hours >= 0 ? "\((hours < 10 && hours >= 0) ? "0" : "")\(hours)" : "--"
        minutesCountLabel.text = minutes >= 0 ? "\((minutes < 10 && minutes >= 0) ? "0" : "")\(minutes)" : "--"
        
        if hours < 0 { print("[WARNING] Negative hours: ", hours) }
        if minutes < 0 { print("[WARNING] Negative minutes: ", minutes) }
        
        let hoursSuffix = hours % 10
        let minutesSuffix = minutes % 10
        
        let oneHourSting = Localization.commonOneHour.getLzText(original: "Час")
        let twoHoursSting = Localization.commonTwoHours.getLzText(original: "Часа")
        let manyHoursSting = Localization.commonManyHours.getLzText(original: "Часов")
        
        let oneMinuteSting = Localization.commonOneMinute.getLzText(original: "Минута")
        let twoMinutesSting = Localization.commonTwoMinutes.getLzText(original: "Минуты")
        let manyMinutesSting = Localization.commonManyMinutes.getLzText(original: "Минут")
        
        if oneHourSting == "Час" {
            if (hours > 4 && hours < 21) || hoursSuffix == 0 || (hoursSuffix >= 5 && hoursSuffix <= 9) {
                hoursLabel.text = manyHoursSting
            } else if hoursSuffix == 1 {
                hoursLabel.text = oneHourSting
            } else {
                hoursLabel.text = twoHoursSting
            }
            
            if (minutes > 4 && minutes < 21) || minutesSuffix == 0 || (minutesSuffix >= 5 && minutesSuffix <= 9) {
                minutesLabel.text = manyMinutesSting
            } else if minutesSuffix == 1 {
                minutesLabel.text = oneMinuteSting
            } else {
                minutesLabel.text = twoMinutesSting
            }
        } else {
            hoursLabel.text = hours == 1 ? oneHourSting : manyHoursSting
            minutesLabel.text = minutes == 1 ? oneMinuteSting : manyMinutesSting
        }
        
    }
    
    private func updateCurrentWordUI() {
        
        currentWordView.setupUI(currentWord)
        
        setTime()
        
        currentWordContainerView.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.currentWordContainerView.alpha = 1
        }
        
        if !history.isEmpty {
            historyPageControl.numberOfPages = history.count
            
            historyContainerView.isHidden = history.isEmpty
            UIView.animate(withDuration: 0.2) {
                self.historyContainerView.alpha = self.history.isEmpty ? 0 : 1
            }
        }
    }
    
    private func setTime() {
        time = currentWord.timeToNextWord
        
        if !isTimerUpdate || time.seconds > 0  {
            startTimer()
        }
    }

    private func updateHistoryUI() {
        
        historyStackView.subviews.forEach { $0.removeFromSuperview() }
        
        for clientWord in history {
            let clientWordView = ClientWordView()
            clientWordView.bottomContentInset = 70
            clientWordView.setupUI(clientWord)
            historyStackView.addArrangedSubview(clientWordView)
            
            clientWordView.widthAnchor.constraint(equalTo: wordsContainerShadowView.widthAnchor, multiplier: 1.0).isActive = true
        }
        
        if currentWord != nil {
            historyPageControl.numberOfPages = history.count
            
            historyContainerView.isHidden = history.isEmpty
            UIView.animate(withDuration: 0.2) {
                self.historyContainerView.alpha = self.history.isEmpty ? 0 : 1
            }
        }
    }
    
    private func showSubscriptionController() {
        let vc = SubscriptionController.storyboardViewController(in: UIStoryboard.storyboars.start)
        vc.hideDemoButton = true
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true)
    }
    
    private func showEmptyWordsAllert() {
        self.showWarningAlert(text: ("words_alert_empty_words", "Слова для изучения закончились. Попробуйте позже"))
    }
    
    private func checkSubscription() {
        
        guard !Global.isMonthlyDemoPeriodEnded else {
            Global.monthlyDemoPeriodEnded = nil
            let vc = SubscribeDiscountController.storyboardViewController(in: UIStoryboard.storyboars.start)
            vc.type = .monthly
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
            return
        }
        
        guard !Global.isDemoPeriodEnded else {
            Global.demoPeriodEnded = nil
            let vc = SubscribeDiscountController.storyboardViewController(in: UIStoryboard.storyboars.start)
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
            return
        }
        
        guard Global.demoPeriodEnded == nil else {
            print("[Word] Demo period activated. Subscription check disabled")
            return
        }
        
        print("[Words] Check subscribe status: \(Apphud.hasActiveSubscription())")
        
        #if !targetEnvironment(simulator)
        guard !Global.foreverAccess else { return }
        guard Apphud.hasActiveSubscriptionForever() else {
            self.showSubscriptionController()
            return
        }
        
        print("[Words] Start checking proccess")
        
        Apphud.restorePurchases { _, _, _  in
            print("[Words] End checking proccess\nhas subscription: \(Apphud.subscription()?.productId ?? "nil")")
            if !Apphud.hasActiveSubscriptionForever() {
                self.showSubscriptionController()
            } else if Apphud.subscription()?.productId == Apphud.Product.monthly.rawValue {
                if Global.monthlyDemoPeriodEnded == nil {
                    Global.startMonthlyDemoPeriod()
                }
            } else {
                Global.monthlyDemoPeriodEnded = nil
            }
        }
        
        #endif
    }
    
    @IBAction private func shareButtonAction(_ sender: UIButton) {
        
        guard let appUrl = URL(string: Global.appLink) else { return }

        let textToShare = [appUrl]

        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)

        activityViewController.popoverPresentationController?.sourceView = self.view

        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction private func repeatButtonAction(_ sender: UIButton) {
        
        guard reachability.connection != .unavailable else {
            self.showAlert(text: Localization.commonInternetConnectionProblem.localized)
            return
        }
        
        guard currentWord != nil else { return }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        API.WordsModule.repeatWord(clientwordId: currentWord.id) { clientWord in
                    
            guard let clientWord = clientWord else {
                self.showEmptyWordsAllert()
                hud.hide(animated: true)
                return
            }

            //            self.history.insert(self.currentWord, at: 0)
            self.refreshHistory {
                hud.hide(animated: true)
                self.currentWord = clientWord
                
                self.showWarningAlert(text: (Localization.wordsRepeatWordSuccess.localized, "Данное слово будет повторяться чаще"), title: ("", ""))
            }
//            self.history.insert(self.currentWord, at: 0)
//            self.currentWord = clientWord
        }
    }
    
    @IBAction private func knowThisWordButtonAction(_ sender: UIButton) {
        
        guard reachability.connection != .unavailable else {
            self.showAlert(text: Localization.commonInternetConnectionProblem.localized)
            return
        }
        
        guard currentWord != nil else { return }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        API.WordsModule.alreadyKnow(clientwordId: currentWord.id) { clientWord in
            
            guard let clientWord = clientWord else {
                self.showEmptyWordsAllert()
                hud.hide(animated: true)
                return
            }
            
//            self.history.insert(self.currentWord, at: 0)
            self.refreshHistory {
                hud.hide(animated: true)
                self.currentWord = clientWord
            }
        }
    }
}

extension WordsController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        historyPageControl.currentPage = Int(pageNumber)
    }
}
