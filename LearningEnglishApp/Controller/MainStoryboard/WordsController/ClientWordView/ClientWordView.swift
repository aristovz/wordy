//
//  ClientWordView.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation

class ClientWordView: NibView {

    @IBOutlet private weak var originalLanguageFlagIcon: UIImageView!
    @IBOutlet private weak var translationLanguageFlagIcon: UIImageView!
    
    @IBOutlet private weak var originalLanguageLabel: UILabel!
    @IBOutlet private weak var translationLanguageLabel: UILabel!
    
    @IBOutlet private weak var originalWordLabel: UILabel!
    @IBOutlet private weak var translationWordLabel: UILabel!
    
    @IBOutlet private weak var originalWordHighlightView: UIView!
    @IBOutlet private weak var translationWordHighlightView: UIView!
    
    @IBOutlet private weak var samplesScrollView: UIScrollView!
    @IBOutlet private weak var samplesStackView: UIStackView!
    
    public var bottomContentInset: CGFloat = 0
    
    private var clientWord: ClientWord!
    private var currentSpeechHighlightView: UIView? = nil
    
    private let speechSynthesizer = AVSpeechSynthesizer()
    
    func setupUI(_ clientWord: ClientWord) {
        
        self.clientWord = clientWord
        
        speechSynthesizer.delegate = self
        
        originalWordLabel.text = clientWord.word.translationWord
        translationWordLabel.text = clientWord.word.originalWord
        
        let originalTapGesture = UITapGestureRecognizer(target: self, action: #selector(speechOriginalWord))
        let translationTapGesture = UITapGestureRecognizer(target: self, action: #selector(speechTranslationWord))
        
        originalWordLabel.isUserInteractionEnabled = true
        originalWordLabel.addGestureRecognizer(originalTapGesture)
        
        translationWordLabel.isUserInteractionEnabled = true
        translationWordLabel.addGestureRecognizer(translationTapGesture)
        
        if let languagePair = clientWord.word.languagePair {
            originalLanguageLabel.text = languagePair.to.name
            translationLanguageLabel.text = languagePair.from.name
            
            originalLanguageFlagIcon.sd_setImage(with: URL(string: languagePair.to.flag), completed: nil)
            translationLanguageFlagIcon.sd_setImage(with: URL(string: languagePair.from.flag), completed: nil)
        }
        
        updateSamplesUI(in: samplesStackView, clientWord: clientWord)
    }
    
    private func updateSamplesUI(in stackView: UIStackView, clientWord: ClientWord) {
        stackView.subviews.forEach { $0.removeFromSuperview() }
        
        let samples = clientWord.word.samples
        
        for k in 0..<samples.count {
            let wordSampleView = WordSampleView()
            
            wordSampleView.tag = k
            wordSampleView.setupUI(samples[k], position: k)
            
            if samples[k].id != -1 {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(speechSampleWord(_:)))
                wordSampleView.addGestureRecognizer(tapGesture)
            }
            
            stackView.addArrangedSubview(wordSampleView)
        }
        
        samplesScrollView.contentInset.bottom = bottomContentInset
    }
    
    private func getHighlightedView(_ utterance: AVSpeechUtterance) -> UIView? {
        
        if utterance.speechString == originalWordLabel.text {
            return originalWordHighlightView
        } else if utterance.speechString == translationWordLabel.text {
            return translationWordHighlightView
        }
        
        guard let sampleIndex = clientWord.word.samples.firstIndex(where: { $0.translationWord == utterance.speechString }) else { return nil }
        
        return samplesStackView.arrangedSubviews.first(where: { $0.tag == sampleIndex })
    }
    
    @objc private func speechOriginalWord() {
        
        guard let text = originalWordLabel.text, !text.isEmpty else { return }
        
        speechSynthesizer.stopSpeaking(at: .immediate)
        
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.rate = 0.4

        currentSpeechHighlightView = originalWordHighlightView
        speechSynthesizer.speak(utterance)
    }

    @objc private func speechTranslationWord() {
        
        guard let text = translationWordLabel.text, !text.isEmpty else { return }
        
        speechSynthesizer.stopSpeaking(at: .immediate)
        
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: "ru")
        utterance.rate = 0.5

        currentSpeechHighlightView = translationWordHighlightView
        speechSynthesizer.speak(utterance)
    }
    
    @objc private func speechSampleWord(_ gestureRecognizer: UITapGestureRecognizer) {
        
        guard let sampleView = gestureRecognizer.view as? WordSampleView else { return }
        
        let sample = clientWord.word.samples[sampleView.tag]
        
        speechSynthesizer.stopSpeaking(at: .immediate)
        
        let utterance = AVSpeechUtterance(string: sample.translationWord)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.rate = 0.4

        currentSpeechHighlightView = sampleView
        speechSynthesizer.speak(utterance)
    }
}

extension ClientWordView: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        print("[AVSpeechSynthesizerDelegate] didStart", utterance.speechString)
        
        let backgroundColor = Pallete.blueGradient.withAlphaComponent(0.2)
        
        UIView.animate(withDuration: 0.2) {
            self.currentSpeechHighlightView?.backgroundColor = backgroundColor
        }
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        print("[AVSpeechSynthesizerDelegate] didFinish", utterance.speechString)
        
        guard let highlightedView = getHighlightedView(utterance) else { return }
        
        UIView.animate(withDuration: 0.2) {
            highlightedView.backgroundColor = .clear
        }
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
        print("[AVSpeechSynthesizerDelegate] didCancel", utterance.speechString)
        
        guard let highlightedView = getHighlightedView(utterance) else { return }
        
        UIView.animate(withDuration: 0.2) {
            highlightedView.backgroundColor = .clear
        }
    }
}
