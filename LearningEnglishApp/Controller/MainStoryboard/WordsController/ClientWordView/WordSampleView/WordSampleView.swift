//
//  WordSampleView.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class WordSampleView: NibView {

    @IBOutlet private weak var colorHoverView: UIView!
    
    @IBOutlet private weak var originalWordLabel: UILabel!
    @IBOutlet private weak var translationWordLabel: UILabel!
    
    func setupUI(_ word: Word, position: Int) {
        
        colorHoverView.backgroundColor = Pallete.wordSampleColor(for: position)
        
        guard word.id != -1 else {
            setupUnvailabelUI(word)
            return
        }
        
        originalWordLabel.text = word.translationWord
        translationWordLabel.text = word.originalWord
        
        translationWordLabel.setLineSpacing(lineHeightMultiple: 1.3)
    }
    
    private func setupUnvailabelUI(_ word: Word) {
        guard let time = word.time?.timeToNextWord else { return }
        
        originalWordLabel.text = "\(Localization.wordsNewExampleWill.localized) \(time.hours)\(Localization.commonMHours.localized) \((time.minutes + 1) < 10 ? "0" : "")\(max(-1, time.minutes) + 1)\(Localization.commonMMinutes.localized)"
        translationWordLabel.text = Localization.wordsSureNotificationsOn.localized
        
        translationWordLabel.setLineSpacing(lineHeightMultiple: 1.3)
    }
}
