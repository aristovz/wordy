//
//  SettingsController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 25.12.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import StoreKit
import ApphudSDK
import MBProgressHUD

class SettingsController: UIViewController {

    // MARK: - IBOutlets
    
    // Trial left view
    @IBOutlet private weak var trialLeftParentView: UIView!
    @IBOutlet private weak var trialLeftContainerView: UIView!
    @IBOutlet private weak var trialLeftLabel: UILabel!
    
    // Select language pair views
    @IBOutlet private weak var selectLanguagePairContainerView: UIView!
    @IBOutlet private weak var selectLevelContainerView: UIView!
    
    @IBOutlet private weak var studyingLanguageNameLabel: UILabel!
    @IBOutlet private weak var studyingLanguageFlagImageView: UIImageView!
    
    @IBOutlet private weak var inLanguageNameLabel: UILabel!
    @IBOutlet private weak var inLanguageFlagImageView: UIImageView!
    
    @IBOutlet private var levelLabels: [UILabel]!
    @IBOutlet private var levelCheckIcons: [UIImageView]!
    
    // Topics collection view
    @IBOutlet private weak var topicsStackView: UIStackView!
    
    // Select periodicaly for push view
    @IBOutlet private weak var selectTimesForPushContainerView: UIView!
    
    @IBOutlet private var startFromTimeStackView: UIStackView!
    @IBOutlet private var endInTimeStackView: UIStackView!
    @IBOutlet private var wordsInDayStackView: UIStackView!
    @IBOutlet private var dayOfWeekStackView: UIStackView!
    
    // Set auto size font fot buttons
    @IBOutlet private var autoFontSizeButtons: [UIButton]!
    
    // MARK: - Local variables
    
    private var gradients: [UIView : CALayer] = [:]
    
    private var studyingLanguage: Language? = nil
    private var inLanguage: Language? = nil
    
    private lazy var languagePairs: [LanguagePair] = LanguagePair.availablePairs
    
    private var weekdays: [String] {
        var calendar = Calendar.current
        calendar.locale = Locale.autoupdatingCurrent
        return calendar.shortWeekdaySymbols
    }
    
    private var isUpdateTimesUIBlocked = false
    
    // MARK: - Selected values for save client settings
    
    private var selectedPair: LanguagePair? {
        return languagePairs.first(where: { $0.to == studyingLanguage && $0.from == inLanguage })
    }
    
    private var selectedLevel: Complexity = .beginner {
        didSet { updateLevelUI() }
    }

    private var selectedStartTime: Int? {
        return startFromTimeStackView.arrangedSubviews.first(where: { ($0 as? UIButton)?.isSelected ?? false })?.tag
    }
    
    private var selectedEndTime: Int? {
        return endInTimeStackView.arrangedSubviews.first(where: { ($0 as? UIButton)?.isSelected ?? false })?.tag
    }
    
    private var selectedCountWordsPerDay: Int? {
        return wordsInDayStackView.arrangedSubviews.first(where: { ($0 as? UIButton)?.isSelected ?? false })?.tag
    }
    
    private var selectedDaysOfWeek: [Int] {
        return dayOfWeekStackView.arrangedSubviews.filter({ ($0 as? UIButton)?.isSelected ?? false }).map { $0.tag }
    }
    
    private var selectedCategories: [Category] = []
    
    // Button for time stack views
    private var stackViewButton: UIButton {
        let button = UIButton()
        
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        
        button.titleLabel?.font = UIFont.getAvenirFont(font: .roman, size: 18)
        
        button.tintColor = .clear
        
        button.setTitleColor(Pallete.SettingsController.stackViewButtonsNormalText, for: .normal)
        button.setTitleColor(Pallete.SettingsController.stackViewButtonsSelectedText, for: .selected)
        
        button.backgroundColor = .clear
        
        return button
    }
    
    // MARK: - Lifecycle functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        updateSettingsUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        #if !targetEnvironment(simulator)
        trialLeftParentView.isHidden = Apphud.trialDaysLeft == nil
        trialLeftLabel.text = "settings_trial_left_\(Apphud.trialDaysLeft ?? 1)".getLzText(original: "{pm} дня демо", parameter: Apphud.trialDaysLeft ?? 1)
//        #endif
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        trialLeftContainerView.setDefaultShadow()
        
        selectLanguagePairContainerView.setDefaultShadow()
        selectLevelContainerView.setDefaultShadow()
        
        selectTimesForPushContainerView.setDefaultShadow()
        
        if !isUpdateTimesUIBlocked {
            updateSettingTimeUI()
        }
    }
    
    
    // MARK: - Private functions for initialization
    
    private func setupUI() {
        
        fillNumbersStackView(startFromTimeStackView, range: 0..<24)
        fillNumbersStackView(endInTimeStackView, range: 0..<24)
        fillNumbersStackView(wordsInDayStackView, range: 1..<24)
        fillDaysOfWeekStackView()
        
        autoFontSizeButtons.forEach {
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    
    private func updateSettingsUI() {
        loadLanguagePairs()
        setupLevelUI()
        
        let client = Client.current
        setupWeekdaysUI(client?.settings.currentWeekdayIndexes ?? [])
        
        let categories = client?.settings.categories ?? []
        selectedCategories = categories
        setupCategoriesUI(categories)
    }
    
    private func updateSettingTimeUI() {
        
        let client = Client.current
        
        if let startHour = client?.settings.hourStart {
            setupTimesUI(startFromTimeStackView, selectedTime: startHour)
        }
        
        if let finishHour = client?.settings.hourFinish {
            setupTimesUI(endInTimeStackView, selectedTime: finishHour)
        }
        
        if let countWordsPerDay = client?.settings.countWordsPerDay {
            setupTimesUI(wordsInDayStackView, selectedTime: countWordsPerDay)
        }
    }
    
    private func fillNumbersStackView(_ stackView: UIStackView, range: Range<Int>) {
        stackView.subviews.forEach { $0.removeFromSuperview() }
        
        for value in range {
            let button = stackViewButton
            
            button.tag = value
            button.setTitle("\(value)", for: .normal)
            
            button.widthAnchor.constraint(equalToConstant: 36).isActive = true
            button.addTarget(self, action: #selector(timeButtonActions(_:)), for: .touchUpInside)
            
            stackView.addArrangedSubview(button)
        }
    }
    
    private func fillDaysOfWeekStackView() {
        dayOfWeekStackView.subviews.forEach { $0.removeFromSuperview() }
        
        let firstWeekday = Calendar.current.firstWeekday
        let normalizeWeekdays = weekdays.rotatedArraySingleLeft(count: firstWeekday - 1)
        
        for k in 0..<normalizeWeekdays.count {
            let button = stackViewButton
            
            button.tag = k
            button.setTitle(normalizeWeekdays[k].firstUppercased, for: .normal)
            
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 7)
            button.addTarget(self, action: #selector(weekdayButtonActions(_:)), for: .touchUpInside)
            
            dayOfWeekStackView.addArrangedSubview(button)
        }
    }
    
    // MARK: - Private functions for update UI
    
    private func loadLanguagePairs() {
        if let pair = languagePairs.first(where: { $0.id == Client.current?.settings.languageId }) {
            self.updateLanguageUI(pair)
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)

        if SharedGlobal.isAuthorized {
           loadLanguagePairsRequest()
        } else {
           API.SystemModule.getToken(needLoadLanguages: false) { _, result in
               guard result else {
                   hud.hide(animated: true)
                   self.showWarningAlert(text: ("launch_error_alert_load_languages", "Произошла ошибка при попытке загрузки языков. Попробуйте позже или обратитесь в службу тех. поддержки"))
                   return
               }
               
               self.loadLanguagePairsRequest()
           }
        }
    }

    private func loadLanguagePairsRequest() {
        API.LanguageModule.getPairs { languagePairs in
            MBProgressHUD.hide(for: self.view, animated: true)
           
            guard !languagePairs.isEmpty else {
                self.showWarningAlert(text: ("launch_error_alert_load_languages", "Произошла ошибка при попытке загрузки языков. Попробуйте позже или обратитесь в службу тех. поддержки"))
                return
            }
           
            self.languagePairs = languagePairs
            
            let currentClient = Client.current
            if let languageId = currentClient?.settings.languageId {
                if let pair = languagePairs.first(where: { $0.id == languageId }) {
                    self.updateLanguageUI(pair)
                } else {
                    currentClient?.settings.languageId = languagePairs.first?.id
                    currentClient?.updateSettings()
                    self.updateLanguageUI(languagePairs.first)
                }
            } else {
                if let pair = languagePairs.first {
                    currentClient?.settings.languageId = languagePairs.first?.id
                    currentClient?.updateSettings()
                    self.updateLanguageUI(pair)
                }
            }
        }
    }
    
    private func updateLanguageUI(_ pair: LanguagePair? = nil) {
        
        if let pair = pair {
            studyingLanguage = pair.to
            inLanguage = pair.from
        }
        
        if let language = studyingLanguage {
            studyingLanguageNameLabel.text = language.name
            studyingLanguageFlagImageView.sd_setImage(with: URL(string: language.flag), completed: nil)
        }
        
        if let language = inLanguage {
            inLanguageNameLabel.text = language.name
            inLanguageFlagImageView.sd_setImage(with: URL(string: language.flag), completed: nil)
        }
    }
    
    private func updateLevelUI() {
        levelLabels.forEach({
            $0.font = UIFont.getAvenirFont(font: $0.tag == selectedLevel.level ? .medium : .roman, size: 18)
        })
        
        levelCheckIcons.forEach({
            $0.image = $0.tag == selectedLevel.level ? #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "unchecked")
        })
    }
    
    private func getLanguages(_ studying: Bool) -> [Language] {
        
        var languages = [Language]()
        
        if studying {
            if let inLanguage = inLanguage {
                languages = languagePairs.filter({ $0.from == inLanguage }).map { $0.to }
            } else {
                languages = languagePairs.map { $0.to }
            }
        } else {
            if let studyingLanguage = studyingLanguage {
                languages = languagePairs.filter({ $0.to == studyingLanguage }).map { $0.from }
            } else {
                languages = languagePairs.map { $0.from }
            }
        }
        
        return languages.unique
    }
    
    private func setupLevelUI() {
        
        let currentClient = Client.current
        if let level = currentClient?.settings.complexity {
            selectedLevel = level
        } else {
            selectedLevel = .beginner
            currentClient?.settings.complexity = .beginner
            currentClient?.updateSettings()
        }
    }
    
    private func checkUpdatingTime(for sender: UIButton) -> Bool {
        
        guard let parentStackView = sender.superview as? UIStackView else { return true }
        
        if parentStackView === startFromTimeStackView {
            if let endTime = selectedEndTime, endTime <= sender.tag {
                self.showWarningAlert(text: (key: Localization.settingsErrorAlertStartBiggerEnd, original: "Время для начала не может быть позже времени окончания"))
                return false
            }
        } else if parentStackView === endInTimeStackView {
            if let startTime = selectedStartTime, startTime >= sender.tag {
                self.showWarningAlert(text: (key: Localization.settingsErrorAlertEndSmallerStart, original: "Время окончания не может быть раньше времени начала"))
                return false
            }
        }
        
        return true
    }
    
    private func setupTimesUI(_ stackView: UIStackView, selectedTime: Int) {
        
        let buttons = stackView.arrangedSubviews.compactMap { $0 as? UIButton }
        
        buttons.forEach {
            $0.isSelected = $0.tag == selectedTime
            $0.backgroundColor = $0.isSelected ?
                Pallete.SettingsController.stackViewButtonsSelectedBackground :
                Pallete.SettingsController.stackViewButtonsNormalBackground
        }
        
        guard let parentScrollView = stackView.superview as? UIScrollView else { return }
        guard let selectedButton = buttons.first(where: { $0.isSelected }) else { return }
        
        let neededCenter = selectTimesForPushContainerView.frame.width / 2
        let buttonCenter = selectedButton.center.x
        
        let contentSize = parentScrollView.contentSize
        let maxContentOffset = contentSize.width - parentScrollView.frame.width
        let contentOffsetX = min(maxContentOffset, max(0, buttonCenter - neededCenter + stackView.frame.minX))
        parentScrollView.setContentOffset(CGPoint(x: contentOffsetX, y: 0), animated: true)
    }
    
    private func setupWeekdaysUI(_ selectedWeekdays: [Int]) {
        
        let buttons = dayOfWeekStackView.arrangedSubviews.compactMap { $0 as? UIButton }
        
        buttons.forEach {
            $0.isSelected = selectedWeekdays.contains($0.tag)
            $0.backgroundColor = $0.isSelected ?
                Pallete.SettingsController.stackViewButtonsSelectedBackground :
                Pallete.SettingsController.stackViewButtonsNormalBackground
        }
    }
    
    private func setupCategoriesUI(_ selectedCategories: [Category]) {
        topicsStackView.subviews.forEach { $0.removeFromSuperview() }
        
        guard !selectedCategories.isEmpty else { return }
        
        for k in 0..<selectedCategories.count where k.isMultiple(of: 2) {
            var categories = [selectedCategories[k]]
            if k + 1 < selectedCategories.count {
                categories.append(selectedCategories[k + 1])
            }
            
            let topicStackView = TopicStackView(categories: categories)
            topicsStackView.addArrangedSubview(topicStackView)
        }
    }
    
    private func returnToOldSettings(_ oldSettings: ClientSettings?) {
        guard let oldSettings = oldSettings else { return }
        
        Client.current?.settings = oldSettings
        updateSettingsUI()
    }
    
    private func updateClientSettings() {
        
        let client = Client.current
        let oldSettings = client?.settings
        
        client?.settings.complexity = selectedLevel
        client?.settings.languageId = selectedPair?.id
        client?.settings.hourStart = selectedStartTime
        client?.settings.hourFinish = selectedEndTime
        client?.settings.countWordsPerDay = selectedCountWordsPerDay
        client?.settings.categories = selectedCategories
        client?.settings.weekdayIndexesForSave = selectedDaysOfWeek
        
//        print(client?.settings.serverSaveMap)
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        client?.updateSettings() { result in
            hud.hide(animated: true)
            
            guard result else {
                self.showWarningAlert(text: ("common_error_alert_save_settings", "Произошла ошибка при попытке сохранить данные. Попробуйте позже или обратитесь в службу тех. поддержки")) {
                    self.returnToOldSettings(oldSettings)
                }
                return
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction private func renewNowButtonAction(_ sender: UIButton) {
        let vc = RenewTrialController.xibViewController()
        self.presentFull(vc, animated: true, completion: nil)
    }
    
    // Select language pair actions
    @IBAction private func selectLanguageButtonsAction(_ sender: UIButton) {
            
        let vc = SelectLanguageController.storyboardViewController(in: UIStoryboard.storyboars.start)
        vc.languages = getLanguages(sender.tag == 0)
        vc.selectedLanguage = sender.tag == 0 ? studyingLanguage : inLanguage
        
        vc.didSelectLanguage = { language in
            if sender.tag == 0 {
                self.studyingLanguage = language
            } else {
                self.inLanguage = language
            }
            
            self.updateLanguageUI()
            self.updateClientSettings()
        }
        
        vc.modalPresentationStyle = .fullScreen
//        vc.modalTransitionStyle = .crossDissolve
        
        self.present(vc, animated: true)
    }
    
    @IBAction private func levelButtonsAction(_ sender: UIButton) {
        guard let level = Complexity.get(from: sender.tag) else { return }
        
        selectedLevel = level
        updateClientSettings()
    }
    
    
    @IBAction private func choseTopicButtonAction(_ sender: UIButton) {
        let vc = TopicsController.storyboardViewController(in: UIStoryboard.storyboars.start)
        
        vc.didSelectCategories = { categories in
            self.selectedCategories = categories
            self.setupCategoriesUI(categories)
            self.updateClientSettings()
        }
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true)
    }
    
    // Schedule for push actions
    @objc private func timeButtonActions(_ sender: UIButton) {
        
        guard checkUpdatingTime(for: sender) else { return }
        guard !sender.isSelected else { return }
        guard let parentStackView = sender.superview as? UIStackView else { return }
        
        isUpdateTimesUIBlocked = true
        
        setupTimesUI(parentStackView, selectedTime: sender.tag)
        updateClientSettings()
    }
    
    @objc private func weekdayButtonActions(_ sender: UIButton) {
        
        if sender.isSelected {
            guard selectedDaysOfWeek.count > 1 else {
                self.showAlert(text: Localization.settingsErrorAlertEmptyWeekdays.localized)
                return
            }
        }
        
        sender.isSelected.toggle()
        sender.backgroundColor = sender.isSelected ?
                    Pallete.SettingsController.stackViewButtonsSelectedBackground :
                    Pallete.SettingsController.stackViewButtonsNormalBackground
        
        updateClientSettings()
    }
    
    // Bottom actions
    @IBAction private func restorePurchasesButtonAction(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        Apphud.restorePurchases { subscriptions, _, error  in
            hud.hide(animated: true)
            
            if Apphud.hasActiveSubscriptionForever() {
                self.showWarningAlert(text: (key: "common_restore_success", original: "Покупки успешно восстановлены"))
            } else {
                self.showWarningAlert(text: (key: "common_restore_failed", original: "Покупки не удалось восстановить"))
            }
        }
    }
    
    @IBAction private func contactDeveloperButtonAction(_ sender: UIButton) {
        let canShowMailController = openMailController([Global.supportEmail])

        if !canShowMailController {
            self.showAlert(text: Localization.commonSetupMailClient.localized)
        }
    }
    
    @IBAction private func onboardingButtonAction(_ sender: UIButton) {
        let vc = OnboardingController.xibViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
//        Global.loadOnboarding(in: self)
    }
    
    @IBAction private func reviewAppButtonAction(_ sender: UIButton) {
        SKStoreReviewController.requestReview()
        
//        guard let url = URL(string: Global.writeReviewLink) else { return }
//        guard UIApplication.shared.canOpenURL(url) else { return }
//
//        UIApplication.shared.open(url)
    }
}
