//
//  TopicView.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 07.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class TopicView: NibView {
    
    @IBOutlet private weak var colorIndicatorView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50)
    }
    
    func setupUI(_ category: Category) {
        titleLabel.text = category.name
        titleLabel.setLineSpacing(lineHeightMultiple: 1.3)
        
        colorIndicatorView.backgroundColor = category.color
        
        titleLabel.numberOfLines = category.name.contains(" ") ? 2 : 1
    }
}
