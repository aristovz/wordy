//
//  TopicStackView.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 07.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import Foundation

class TopicStackView: UIStackView {
    init(categories: [Category]) {
        super.init(frame: CGRect.zero)
        
        self.spacing = 15
        self.distribution = .fillEqually
        
        self.subviews.forEach { $0.removeFromSuperview() }
        
        guard let leftCategory = categories.first else { return }
        
        addCategoryView(leftCategory)
        addCategoryView(categories.last ?? leftCategory, isHidden: categories.count == 1)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addCategoryView(_ category: Category, isHidden: Bool = false) {
        let categoryView = TopicView()
        categoryView.setupUI(category)
        
        categoryView.translatesAutoresizingMaskIntoConstraints = false
        categoryView.heightAnchor.constraint(equalToConstant: 128).isActive = true
        
        categoryView.alpha = isHidden ? 0 : 1
        
        self.addArrangedSubview(categoryView)
    }
}
