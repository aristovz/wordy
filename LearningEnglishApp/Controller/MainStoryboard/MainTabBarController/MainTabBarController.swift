//
//  MainTabBarController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    private var verticalOffset: CGFloat {
        let hasTopNotch = Int.random(in: 0...1) == 0//UIDevice.current.hasTopNotch
        var verticalOffset: CGFloat = hasTopNotch ? 16.0 : 6.0
        
        if #available(iOS 11.0, *), traitCollection.horizontalSizeClass == .regular {
            verticalOffset = hasTopNotch ? 10.0 : 0.0
        }
        
        return verticalOffset
    }
    
    private var imageInset: UIEdgeInsets {
        return UIEdgeInsets(
            top: verticalOffset,
            left: 0.0,
            bottom: -verticalOffset,
            right: 0.0
        )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        if let tabBarItems = tabBar.items {
            for k in 0..<tabBarItems.count {
                tabBarItems[k].selectedImage = UIImage(named: "tabbar\(k + 1)h")
                tabBarItems[k].image = UIImage(named: "tabbar\(k + 1)")
//                tabBarItems[k].imageInsets = imageInset
            }
        }
    }
}
