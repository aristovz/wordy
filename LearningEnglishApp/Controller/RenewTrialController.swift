//
//  RenewTrialController.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 08.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import ApphudSDK
import MBProgressHUD

class RenewTrialController: UIViewController {

    @IBOutlet private weak var trialDaysCountLabel: UILabel!
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var triangleImageView: UIImageView!
    
    @IBOutlet private weak var planTitleLabel: UILabel!
    
    @IBOutlet private weak var expirationLabel: UILabel!
    @IBOutlet private weak var expirationDetailLabel: UILabel!
    
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var priceDetailLabel: UILabel!
    
    @IBOutlet private weak var savingContainerView: UIView!
    @IBOutlet private weak var savingLabel: UILabel!
    @IBOutlet private weak var savingDetailLabel: UILabel!
    
    @IBOutlet private var pricingViews: [UIView]!
    
    @IBOutlet private weak var slider: StepperSlider!
    
    @IBOutlet private weak var subscribeInformationContainerView: UIView!
    
    @IBOutlet private var underlineButtons: [UIButton]!
    
    private var selectedProduct: Apphud.Product {
        switch slider.selectedIndex {
        case 0: return .monthly
        case 1: return .yearly
        case 2: return .forever
        default:
            print("Undefind planIndex")
            return .yearly
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dropShadow(color: Pallete.RenewTrialController.shadow, opacity: 0.35, offSet: CGSize(width: 0, height: 30), radius: 50)
        triangleImageView.dropShadow(color: Pallete.RenewTrialController.shadow, opacity: 0.35, offSet: CGSize(width: 0, height: 30), radius: 50)
        subscribeInformationContainerView.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50)
    }
    
    private func setupUI() {
        
        trialDaysCountLabel.text = "renew_trial_left_\(Apphud.trialDaysLeft ?? 1)".getLzText(original: "Осталось еще {pm} дня демо", parameter: Apphud.trialDaysLeft ?? 1)
        
        slider.selectedIndexChanged = { index in
            
            self.savingContainerView.isHidden = index != 1
            self.updatePriceUI()
            
            UISelectionFeedbackGenerator().selectionChanged()
            
            guard let pricingView = self.pricingViews.first(where: { $0.tag == index }) else { return }
            
//            self.triangleImageView.translatesAutoresizingMaskIntoConstraints = false
            self.triangleImageView.center.x = pricingView.center.x
        }
        
        underlineButtons.forEach {
            $0.underline()
        }
        
        updatePriceUI()
    }
    
    private func updatePriceUI() {
        if slider.selectedIndex == 0 {
            planTitleLabel.text = "renew_month_title".getLzText(original: "Monthly subscription")
            
            expirationLabel.text = "renew_month_expiration".getLzText(original: "1")
            expirationDetailLabel.text = "renew_month_expiration_detail".getLzText(original: "Month")
            
            priceLabel.text = "renew_month_price".getLzText(original: "$0,064")
            priceDetailLabel.text = "renew_month_price_detail".getLzText(original: "Per Day")
            
        } else if slider.selectedIndex == 1 {
            planTitleLabel.text = "renew_year_title".getLzText(original: "Year subscription")
            
            expirationLabel.text = "renew_year_expiration".getLzText(original: "1")
            expirationDetailLabel.text = "renew_year_expiration_detail".getLzText(original: "Year")
            
            priceLabel.text = "renew_year_price".getLzText(original: "$0,035")
            priceDetailLabel.text = "renew_year_price_detail".getLzText(original: "Per Day")
            
            savingLabel.text = "renew_year_saving".getLzText(original: "10,89")
            savingDetailLabel.text = "renew_year_saving_detail".getLzText(original: "USD")
            
        } else if slider.selectedIndex == 2 {
            planTitleLabel.text = "renew_forever_title".getLzText(original: "Forever subscription")
            
            expirationLabel.text = "renew_forever_expiration".getLzText(original: "😎")
            expirationDetailLabel.text = "renew_forever_expiration_detail".getLzText(original: "Forever")
            
            priceLabel.text = "renew_forever_price".getLzText(original: "$19,99")
            priceDetailLabel.text = "renew_forever_price_detail".getLzText(original: "USD")
        }
    }
    
    private func showForeverAlert() {
        let alert = UIAlertController(title: "Успешно", message: "Вы успешно перешли на режим «Вечный»!\nК сожалению мы не можем отменить вашу подписку сами. Для отмены действующей подписки и перехода полностью на режим «Вечный» Вам нужно отменить подписку самостоятельно", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { _ in
            self.dismiss(animated: true)
        }
        
        let settingsAction = UIAlertAction(title: "Настройки", style: .default) { _ in
            self.dismiss(animated: true) {
                self.openURL(string: "https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions")
            }
        }
        
        alert.addAction(cancelAction)
        alert.addAction(settingsAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction private func subscribeButtonAction(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        var hasProduct = true
        
        Apphud.purchaseProduct(product: selectedProduct, hasProduct: { result in
            hasProduct = result
        }) { subscription, error in
            
            hud.hide(animated: true)
            
            guard error == nil else {
                self.showAlert(text: error ?? "Undefind Error")
                return
            }
            
            if let _ = subscription {
                self.dismiss(animated: true, completion: nil)
            } else {
                guard hasProduct else {
                    self.showWarningAlert(text: (key: "common_product_not_found", original: "Продукт не найден"))
                    return
                }
                
                Global.foreverAccess = true
                self.showForeverAlert()
            }
        }
    }
    
    @IBAction private func restorePurchasesButtonAction(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        Apphud.restorePurchases { subscriptions, _, error  in
            hud.hide(animated: true)
            
            if Apphud.hasActiveSubscriptionForever() {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showWarningAlert(text: (key: "common_restore_failed", original: "Покупки не удалось восстановить"))
            }
        }
    }
    
    @IBAction private func termsOfUseButtonAction(_ sender: UIButton) {
        openURL(string: Global.termsOfUseLink)
    }
    
    @IBAction private func privacyPolicyButtonAction(_ sender: UIButton) {
        openURL(string: Global.privacyPolicyLink)
    }
    
}
