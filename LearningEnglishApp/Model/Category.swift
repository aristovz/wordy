//
//  Category.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 03.12.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import Foundation

class Category: Equatable {
    
    let id: Int
    
    let languageId: Int
    let name: String
    let color: UIColor
    let isRecommended: Bool
    
    init?(from map: [String: Any]) {
        guard let id = map["id"] as? Int,
                let languageId = map["languageId"] as? Int,
                let name = map["name"] as? String,
                let colorHex = map["color"] as? String,
                let isRecommended = map["isRecommended"] as? Bool else {
            print("Can't init Category")
            return nil
        }
        
        self.id = id
        self.languageId = languageId
        self.name = name
        self.color = UIColor(hexString: colorHex)
        self.isRecommended = isRecommended
    }
    
    var map: [String: Any] {
        return [
            "id": id,
            "languageId": languageId,
            "name": name,
            "color": color.toHexString(),
            "isRecommended": isRecommended
        ]
    }
    
    static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.id == rhs.id
    }
}
