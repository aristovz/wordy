//
//  Language.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 19.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import Foundation
import UIKit

class LanguagePair {
    let id: Int
    let from: Language
    let to: Language
    let samples: [Word]
    
    init?(from map: [String: Any]) {
        guard let id = map["id"] as? Int else {
            print("Can't init LanguagePair")
            return nil
        }
        
        guard let fromLanguage = LanguagePair.getLanguage(from: map, key: "from") else { return nil }
        guard let toLanguage = LanguagePair.getLanguage(from: map, key: "to") else { return nil }
        
        self.id = id
        self.from = fromLanguage
        self.to = toLanguage
        
        if let samplesMap = map["samples"] as? [[String: Any]] {
            self.samples = samplesMap.compactMap { Word.map(from: $0) }
        } else {
            self.samples = []
        }
    }
    
    private static func getLanguage(from map: [String: Any], key: String) -> Language? {
        guard let languageMap = map[key] as? [String: Any] else {
            print("[LanguagePair.init] Can't get `Language` map by key: \(key)")
            return nil
        }
        
        return Language(from: languageMap)
    }
    
    static var availablePairs: [LanguagePair] = [] {
        didSet {
            print("[Global] Available Languages Pairs count: \(availablePairs.count)")
        }
    }
}

class Language: Equatable {

    let name: String
    let flag: String
    
    init?(from map: [String: Any]) {
        guard let name = map["name"] as? String,
                let flag = map["flag"] as? String else {
            print("Can't init Language")
            return nil
        }
        
        self.name = name
        self.flag = flag
    }
        
    static func == (lhs: Language, rhs: Language) -> Bool {
        return lhs.name == rhs.name
    }
}

//    static var all: [Language] {
//        return [
//            Language(name: "Africa", flag: #imageLiteral(resourceName: "us")),
//            Language(name: "Albania", flag: #imageLiteral(resourceName: "al")),
//            Language(name: "Argentina", flag: #imageLiteral(resourceName: "ar")),
//            Language(name: "Bangladesh", flag: #imageLiteral(resourceName: "bd")),
//            Language(name: "Belgium", flag: #imageLiteral(resourceName: "be")),
//            Language(name: "Brazil", flag: #imageLiteral(resourceName: "br")),
//            Language(name: "Canada", flag: #imageLiteral(resourceName: "ca")),
//            Language(name: "Chile", flag: #imageLiteral(resourceName: "cl")),
//            Language(name: "Czech Republic", flag: #imageLiteral(resourceName: "cz")),
//            Language(name: "Denmark", flag: #imageLiteral(resourceName: "dk")),
//            Language(name: "Russia", flag: #imageLiteral(resourceName: "ru"))
//        ]
//    }
