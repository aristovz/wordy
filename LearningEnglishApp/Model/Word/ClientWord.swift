//
//  ClientWord.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation

class ClientWord {
    
    let id: Int
    let word: Word
    
    let timeout: Int
    let startLearningTime: Date
    
    var timeToNextWord: (hours: Int, minutes: Int, seconds: Int) {
        
        guard let dateToNextWord = Calendar.current.date(byAdding: .minute, value: timeout, to: startLearningTime) else {
            print("Can't get date in \(#function)")
            return (0, 0, 0)
        }
        
        let allSeconds = Int(dateToNextWord.timeIntervalSince(Date()))
        let hours = Int(allSeconds / 60 / 60)
        let minutes = Int((allSeconds - hours * 60 * 60) / 60)
        let seconds = Int((allSeconds - hours * 60 * 60 - minutes * 60))
        
//        print(startLearningTime)
//        print(dateToNextWord)
//        print(Date())
//        print(timeout, "Timeout")
//        print(allSeconds, "Всего секунд")
//        print(seconds, "секунд")
        
        return (hours, minutes + 1, seconds)
    }
    
    static var dateFormatter: DateFormatter { // 2020-01-20 16:30:02
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }
    
    init?(from map: [String: Any]) {
        guard let id = map["id"] as? Int,
                let timeout = map["timeout"] as? Int,
                let startLearningTimeString = map["startLearning"] as? String,
                let startLearningTime = ClientWord.dateFormatter.date(from: startLearningTimeString),
                let wordMap = map["word"] as? [String: Any],
                let word = Word.map(from: wordMap) else {
            print("Can't init ClientWord")
            return nil
        }
        
        self.id = id
        self.word = word
        
        self.timeout = timeout
        self.startLearningTime = startLearningTime
    }
    
    var map: [String: Any] {
        return [
            "id": id,
            "timeout": timeout,
            "startLearning": ClientWord.dateFormatter.string(from: startLearningTime),
            "word": word.map
        ]
    }
}

extension ClientWord: Equatable {
    static func == (lhs: ClientWord, rhs: ClientWord) -> Bool {
        
        guard lhs.id == rhs.id else { return false }
        
        let lhsSamples = lhs.word.samples.filter({ $0.id == -1 })
        let rhsSamples = rhs.word.samples.filter({ $0.id == -1 })
        
        return lhsSamples.count == rhsSamples.count
    }
}
