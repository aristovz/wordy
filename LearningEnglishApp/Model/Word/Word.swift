//
//  Word.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 25.12.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import Foundation

enum Complexity: String {
    case beginner, intermediate, advance
    
    static var all: [Complexity] {
        return [beginner, intermediate, advance]
    }
    
    var level: Int {
        switch self {
        case .beginner: return 0
        case .intermediate: return 1
        case .advance: return 2
        }
    }
        
    static func get(from level: Int) -> Complexity? {
        return all.first(where: { $0.level == level })
    }
}

enum WordType: String {
    case word, phrase
}

class Word {
    
    let id: Int
    let languageId: Int
    let categoryId: Int
    let wordId: Int?
    let originalWord: String
    let translationWord: String
    let type: WordType
    let transcription: String
    let complexity: Complexity
    
    let languagePair: LanguagePair?
    
    let samples: [Word]
    
    var time: Time? = nil
    
    static func map(from map: [String: Any]) -> Word? {
        guard let id = map["id"] as? Int else { return nil }
        
        guard id != -1 else {
            
            guard let time = Time(from: map) else {
                return nil
            }
            
            return Word(time: time)
        }
        
        guard let languageId = map["languageId"] as? Int,
                let categoryId = map["categoryId"] as? Int,
                let originalWord = map["originalWord"] as? String,
                let translationWord = map["translationWord"] as? String,
                let typeIdentifier = map["type"] as? String,
                let type = WordType(rawValue: typeIdentifier),
                let transcription = map["transcription"] as? String,
                let complexityIdentifier = map["complexity"] as? String,
                let complexity = Complexity(rawValue: complexityIdentifier) else {
            print("Can't init Word")
            return nil
        }
        
        let wordId = map["wordId"] as? Int
        
        let samplesMap = map["words"] as? [[String: Any]]
        let samples = samplesMap?.compactMap { Word.map(from: $0) } ?? []
        
        var languagePair: LanguagePair? = nil
        if let languagePairMap = map["language"] as? [String: Any] {
            languagePair = LanguagePair(from: languagePairMap)
        }
        
        let word = Word(id: id, languageId: languageId, categoryId: categoryId, wordId: wordId, originalWord: originalWord, translationWord: translationWord, type: type, transcription: transcription, complexity: complexity, languagePair: languagePair, samples: samples)
        
        return word
    }

    internal init(id: Int, languageId: Int, categoryId: Int, wordId: Int?, originalWord: String, translationWord: String, type: WordType, transcription: String, complexity: Complexity, languagePair: LanguagePair?, samples: [Word]) {
        self.id = id
        self.languageId = languageId
        self.categoryId = categoryId
        self.wordId = wordId
        self.originalWord = originalWord
        self.translationWord = translationWord
        self.type = type
        self.transcription = transcription
        self.complexity = complexity
        self.languagePair = languagePair
        self.samples = samples
    }
    
    internal init(time: Time) {
        self.time = time
        
        self.id = -1
        self.languageId = -1
        self.categoryId = -1
        self.wordId = -1
        self.originalWord = ""
        self.translationWord = ""
        self.type = .word
        self.transcription = ""
        self.complexity = .beginner
        self.languagePair = nil
        self.samples = []
    }
    
    var map: [String: Any] {
        return [
            "id": id,
            "languageId": languageId,
            "categoryId": categoryId,
            "originalWord": originalWord,
            "translationWord": translationWord,
            "type": type.rawValue,
            "transcription": transcription,
            "complexity": complexity.rawValue
        ]
    }
}

extension Word {
    struct Time {
        let timeout: Int
        let startLearningTime: Date
        
        var timeToNextWord: (hours: Int, minutes: Int, seconds: Int) {
                
            guard let dateToNextWord = Calendar.current.date(byAdding: .minute, value: 0, to: startLearningTime) else {
                print("Can't get date in \(#function)")
                return (0, 0, 0)
            }
            
            let allSeconds = Int(dateToNextWord.timeIntervalSince(Date()))
            let hours = Int(allSeconds / 60 / 60)
            let minutes = Int((allSeconds - hours * 60 * 60) / 60)
            let seconds = Int((allSeconds - hours * 60 * 60 - minutes * 60))
            
    //        print(startLearningTime)
    //        print(dateToNextWord)
    //        print(Date())
    //        print(timeout, "Timeout")
    //        print(allSeconds, "Всего секунд")
    //        print(seconds, "секунд")
            
            return (hours, minutes, seconds)
        }
        
        init?(from map: [String: Any]) {
            guard let timeout = map["timeout"] as? Int,
                    let startLearningTimeString = map["startLearning"] as? String,
                    let startLearningTime = ClientWord.dateFormatter.date(from: startLearningTimeString) else {
                print("Can't init Word.Time")
                return nil
            }
            
            self.timeout = timeout
            self.startLearningTime = startLearningTime
        }
    }
}
