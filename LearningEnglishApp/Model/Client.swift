//
//  Client.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 03.12.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import Foundation

struct ClientSettings: Equatable {
    var languageId: Int?
    var complexity: Complexity?
    var hourStart: Int?
    var hourFinish: Int?
    var countWordsPerDay: Int?
    
    var categories: [Category]
    
    private var allWeekdays: [String] {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en")
        
        let firstWeekday = Calendar.current.firstWeekday
        let allWeekdays = dateFormatter.shortWeekdaySymbols
                                        .map({ $0.dropLast(1).lowercased() })
                                        .rotatedArraySingleLeft(count: firstWeekday - 1)
        
        return allWeekdays
    }
    
    let weekdays: [String]
    var currentWeekdayIndexes: [Int] {
        return weekdays.compactMap { allWeekdays.firstIndex(of: $0) }
    }
    
    var weekdayIndexesForSave: [Int] = []
    private var weekdaysForSave: [String] {
        return weekdayIndexesForSave.compactMap { allWeekdays.elemet(at: $0) }
    }
    
    init(from map: [String: Any]) {
        self.languageId = map["languageId"] as? Int
        self.hourStart = map["hourStart"] as? Int
        self.hourFinish = map["hourFinish"] as? Int
        self.countWordsPerDay = map["countWordsPerDay"] as? Int
        
        self.weekdays = map["weekdays"] as? [String] ?? []
        
        if let complexityIdentifier = map["complexity"] as? String {
            self.complexity = Complexity(rawValue: complexityIdentifier)
            if complexity == nil { print("Can't init Complexity") }
        } else {
            self.complexity = nil
        }
        
        if let categoriesMap = map["categories"] as? [[String: Any]] {
            self.categories = categoriesMap.compactMap { Category(from: $0) }
        } else {
            self.categories = []
        }
        
        self.weekdayIndexesForSave = currentWeekdayIndexes
    }
    
    var serverSaveMap: [String: Any] {
        var result = [String: Any]()
        
        if let languageId = languageId {
            result["languageId"] = languageId
        }
        
        if let complexity = complexity {
            result["complexity"] = complexity.rawValue
        }
        
        if let hourStart = hourStart {
            result["hourStart"] = hourStart
        }
        
        if let hourFinish = hourFinish {
            result["hourFinish"] = hourFinish
        }
        
        if let countWordsPerDay = countWordsPerDay {
            result["countWordsPerDay"] = countWordsPerDay
        }
        
        result["categories"] = categories.map { $0.id }
        result["weekdays"] = weekdaysForSave
        
        return result
    }
        
    static func == (lhs: ClientSettings, rhs: ClientSettings) -> Bool {
        return lhs.languageId == rhs.languageId &&
                lhs.complexity == rhs.complexity &&
                lhs.hourStart == rhs.hourStart &&
                lhs.hourFinish == rhs.hourFinish &&
                lhs.weekdays == rhs.weekdays &&
                lhs.categories == rhs.categories
    }
}

class Client {
    let id: Int
//    let registrationId: String
    let name: String?
    let tariff: String?
    let endPaymentDate: Date?
    let timezone: String?
    
    var settings: ClientSettings
    
    private static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }
    
    init?(from map: [String: Any]) {
        guard let id = map["id"] as? Int else {
            print("Can't init Client")
            return nil
        }
        
        self.id = id
//        self.registrationId = registrationId
        self.name = map["name"] as? String
        self.tariff = map["tariff"] as? String
        self.endPaymentDate = Client.dateFormatter.date(from: map["endPaymentDate"] as? String ?? "")
        self.timezone = map["timezone"] as? String
        
        self.settings = ClientSettings(from: map)
    }
    
    var map: [String: Any] {
        var res: [String: Any] = [
            "id": id,
//            "registrationId": registrationId,
            "categories": settings.categories.map { $0.map },
            "weekdays": settings.weekdays
        ]
        
        if let name = name {
            res["name"] = name
        }
        
        if let languageId = settings.languageId {
            res["languageId"] = languageId
        }
        
        if let tariff = tariff {
            res["tariff"] = tariff
        }
        
        if let endPaymentDate = endPaymentDate {
            res["endPaymentDate"] = Client.dateFormatter.string(from: endPaymentDate)
        }
        
        if let complexity = settings.complexity {
            res["complexity"] = complexity.rawValue
        }
        
        if let timezone = timezone {
            res["timezone"] = timezone
        }
        
        if let hourStart = settings.hourStart {
            res["hourStart"] = hourStart
        }
        
        if let hourFinish = settings.hourFinish {
            res["hourFinish"] = hourFinish
        }
        
        if let countWordsPerDay = settings.countWordsPerDay {
            res["countWordsPerDay"] = countWordsPerDay
        }
        
        return res
    }
    
    func updateSettings(_ completion: ((Bool) -> Void)? = nil) {
        API.ClientModule.saveSettings(self.settings, completion: completion)
    }
}

extension Client {
    
    static private(set) var current: Client? {
        get {
            guard let currentClientMap = UserDefaults.standard.value(forKey: "currentClientMap") as? [String: Any] else {
                return nil
            }
            
            return Client(from: currentClientMap)
        }
        set {
            if newValue != nil {
                print("[Client] Current Client is set")
            }
            
            UserDefaults.standard.set(newValue?.map, forKey: "currentClientMap")
        }
    }
    
    func setAsCurrentClient() {
        Client.current = self
    }
}
