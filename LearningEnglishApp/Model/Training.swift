//
//  Training.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 07.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation

class Training {
    let originalWord: String
    let answers: [String]
    
    init?(from map: [String: Any]) {
        guard let originalWord = map["originalWord"] as? String,
                let answers = map["answers"] as? [String] else {
            print("Can't init Training")
            return nil
        }
        
        self.originalWord = originalWord
        self.answers = answers
    }
    
    func getAnswer(at index: Int) -> String? {
        guard index >= 0 && index < answers.count else { return nil }
        return answers[index]
    }
}
