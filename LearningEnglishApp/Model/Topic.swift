//
//  Topic.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 20.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import Foundation

class Topic {
    let title: String
    let color: UIColor
    
    init(title: String, color: UIColor) {
        self.title = title
        self.color = color
    }
    
    static func getData() -> [[Topic]] {
        return [
            [
                Topic(title: "Top 50\nAdjectives Begin..", color: UIColor(hexString: "0079EC")),
                Topic(title: "Top 50\nPreposition Begin..", color: UIColor(hexString: "E27BFF"))
            ],
            [
                Topic(title: "Family & Relationship", color: UIColor(hexString: "FF3E2B")),
                Topic(title: "Religion", color: UIColor(hexString: "FFBA2B")),
                Topic(title: "Pets", color: UIColor(hexString: "20D996")),
                Topic(title: "Video Games", color: UIColor(hexString: "007FEE")),
                Topic(title: "Adventure", color: UIColor(hexString: "FF7E6D")),
//                Topic(title: "Cinema", color: UIColor(hexString: "404040"))
            ]
        ]
    }
}
