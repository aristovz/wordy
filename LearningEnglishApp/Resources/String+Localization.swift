import Foundation

struct Localization {
	/// Язык
	static let commonLanguage = "common_language"

	/// Изучаю
	static let commonStudying = "common_studying"

	/// Изучаю
	static let commonFrom = "common_from"

	/// Через
	static let commonTo = "common_to"

	/// Через
	static let commonIn = "common_in"

	/// Час
	static let commonOneHour = "common_one_hour"

	/// Часа
	static let commonTwoHours = "common_two_hours"

	/// Часов
	static let commonManyHours = "common_many_hours"

	/// Минута
	static let commonOneMinute = "common_one_minute"

	/// Минуты
	static let commonTwoMinutes = "common_two_minutes"

	/// Минут
	static let commonManyMinutes = "common_many_minutes"

	/// м
	static let commonMMinutes = "common_m_minutes"

	/// ч
	static let commonMHours = "common_m_hours"

	/// Твой уровень
	static let commonYourLevel = "common_your_level"

	/// Новичок
	static let commonBeginner = "common_beginner"

	/// Средний
	static let commonIntermediate = "common_intermediate"

	/// Продвинутый
	static let commonAdvanced = "common_advanced"

	/// ОK
	static let commonOk = "common_ok"

	/// Внимание
	static let commonWarning = "common_warning"

	/// Продолжить
	static let commonContinue = "common_continue"

	/// Подписаться сейчас
	static let commonSubscribeNow = "common_subscribe_now"

	/// Активировать сейчас
	static let commonActiveNow = "common_active_now"

	/// Подписаться
	static let commonSubscribe = "common_subscribe"

	/// Восстановить покупки
	static let commonRestorePurchases = "common_restore_purchases"

	/// Правила пользования
	static let commonTermsOfUse = "common_terms_of_use"

	/// Политика конфиденциальности
	static let commonPrivacyPolicy = "common_privacy_policy"

	/// Отсутствует подключение к интернету.
	static let commonInternetConnectionProblem = "common_internet_connection_problem"

	/// Произошла ошибка при попытке загрузки категорий. Попробуйте позже или обратитесь в службу тех. поддержки
	static let commonErrorAlertLoadCategories = "common_error_alert_load_categories"

	/// Произошла ошибка при попытке сохранить данные. Попробуйте позже или обратитесь в службу тех. поддержки
	static let commonErrorAlertSaveSettings = "common_error_alert_save_settings"

	/// Пожалуйста настройте ваш почтовый клиент
	static let commonSetupMailClient = "common_setup_mail_client"

	/// Демо-доступ завершен!
	static let commonDemoPeriodExpiredPushTitle = "common_demoPeriodExpiredPush_title"

	/// Оплати годовую подписку с 30% скидкой
	static let commonDemoPeriodExpiredPushText = "common_demoPeriodExpiredPush_text"

	/// Специальное предложение
	static let commonMonthlyDemoPeriodExpiredPushTitle = "common_monthlyDemoPeriodExpiredPush_title"

	/// Только сейчас вы можете перейти на более выгодный тариф с 30% скидкой
	static let commonMonthlyDemoPeriodExpiredPushText = "common_monthlyDemoPeriodExpiredPush_text"

	/// Покупки успешно восстановлены
	static let commonRestoreSuccess = "common_restore_success"

	/// Покупки не удалось восстановить
	static let commonRestoreFailed = "common_restore_failed"

	/// Продукт не найден
	static let commonProductNotFound = "common_product_not_found"

	/// Подписка производится с вашего аккаунта iTunes и обновляется автоматически. Отмена подписки производится в основных настройках iOS в параметрах учетной записи iTunes не менее чем за 24 часа до окончания текущего периода. Стоимость подписки будет списана в течение 24 часов после окончания пробного периода. Любая неиспользованная часть бесплатного пробного периода, если таковая предлагается, будет аннулирована, когда пользователь приобретает подписку на эту публикацию, где это применимо. Удаление приложения не отменяет подписку автоматически.
	static let commonSubscribeInformation = "common_subscribe_information"

	/// Разрешть
	static let onboardingButtonAllow = "onboarding_button_allow"

	/// Настройки
	static let onboardingButtonSettings = "onboarding_button_settings"

	/// Продолжить
	static let onboardingButtonContinue = "onboarding_button_continue"

	/// Учи слова без усилий с помощью уведомлений
	static let onboarding1Title = "onboarding_1_title"

	/// Пропускай знакомые слова и повторяй новые слова чаще
	static let onboarding2Title = "onboarding_2_title"

	/// Тренируйте изученные слова!
	static let onboarding3Title = "onboarding_3_title"

	/// Выбирайте нужные вам темы для изучения
	static let onboarding4Title = "onboarding_4_title"

	/// Выбирайте удобное время для изучения
	static let onboarding5Title = "onboarding_5_title"

	/// Разреши получать уведомления для изучения слов
	static let onboarding6Title = "onboarding_6_title"

	/// Утром мы отправим слово, и затем это же слово будет повторяться в контексте очень популярных фраз. Учите до 20 слов в день!
	static let onboarding1Desc = "onboarding_1_desc"

	/// Мы рекомендуем не пропускать слова, так как подбираем разнообразные примеры. Но некоторые слова вы  можете повторять чаще, нажимая на кнопку потворения
	static let onboarding2Desc = "onboarding_2_desc"

	/// Следующий пуш будет через
	static let wordsTitle = "words_title"

	/// Слова
	static let wordsTabbarTitle = "words_tabbar_title"

	/// Слова на сегодня
	static let wordsWordsToday = "words_words_today"

	/// Уже знаю это слово
	static let wordsAlreadyKnow = "words_already_know"

	/// Изученные слова
	static let wordsLearnedWords = "words_learned_words"

	/// Новый пример будет через
	static let wordsNewExampleWill = "words_new_example_will"

	/// Убедись, что ты включил уведомления
	static let wordsSureNotificationsOn = "words_sure_notifications_on"

	/// Слова для изучения закончились. Попробуйте позже
	static let wordsAlertEmptyWords = "words_alert_empty_words"

	/// Мы будем повторять данное слово чаще для лучшего повторения!
	static let wordsRepeatWordSuccess = "words_repeatWord_success"

	/// Настройки
	static let settingsTitle = "settings_title"

	/// Настройки
	static let settingsTabbarTitle = "settings_tabbar_title"

	/// Последний день демо
	static let settingsTrialLeft0 = "settings_trial_left_0"

	/// 1 день демо ✌️
	static let settingsTrialLeft1 = "settings_trial_left_1"

	/// 2 дня демо ✌️
	static let settingsTrialLeft2 = "settings_trial_left_2"

	/// 3 дня демо ✌️
	static let settingsTrialLeft3 = "settings_trial_left_3"

	/// Продлить
	static let settingsRenewNow = "settings_renew_now"

	/// Темы для изучения
	static let settingsTopicForLearning = "settings_topic_for_learning"

	/// Выбрать
	static let settingsChooseTopics = "settings_choose_topics"

	/// Когда вам удобно изучать язык?
	static let settingsSetTheConvenientTime = "settings_set_the_convenient_time"

	/// Начать с ☝️
	static let settingsStart = "settings_start"

	/// Закончить 😎
	static let settingsEnd = "settings_end"

	/// Сколько слов в день 🤔
	static let settingsHowManyWordsADay = "settings_how_many_words_a_day"

	/// Какие дни недели 🙄
	static let settingsWhatDaysOfTheWeek = "settings_what_days_of_the_week"

	/// Время для начала не может быть позже времени окончания
	static let settingsErrorAlertStartBiggerEnd = "settings_error_alert_start_bigger_end"

	/// Время окончания не может быть раньше времени начала
	static let settingsErrorAlertEndSmallerStart = "settings_error_alert_end_smaller_start"

	/// Нужно оставить хотя бы один день недели
	static let settingsErrorAlertEmptyWeekdays = "settings_error_alert_empty_weekdays"

	/// Восстановить покупки 🤔
	static let settingsRestorePurchases = "settings_restore_purchases"

	/// Связаться с разработчиком 👨🏻‍💻
	static let settingsContactDeveloper = "settings_contact_developer"

	/// Обучение 👆
	static let settingsOnboarding = "settings_onboarding"

	/// Оценить приложение 👍
	static let settingsReviewApp = "settings_review_app"

	/// Тренировка
	static let trainingTitle = "training_title"

	/// Тренировка
	static let trainingTabbarTitle = "training_tabbar_title"

	/// На изучение
	static let trainingLearn = "training_learn"

	/// Выберите ответ
	static let trainingChooseOneOfWords = "training_choose_one_of_words"

	/// Следующее слово
	static let trainingGetNextWord = "training_get_next_word"

	/// Доступные тренировки закончились. Попробуйте позже
	static let trainingAlertEmptyWords = "training_alert_empty_words"

	/// Вы дали правильный ответ!
	static let trainingAlertResultAnswerCorrect = "training_alert_result_answer_correct"

	/// Вы дали неправильный ответ!
	static let trainingAlertResultAnswerWrong = "training_alert_result_answer_wrong"

	/// Вы можете использовать приложение последний день
	static let renewTrialLeft0 = "renew_trial_left_0"

	/// Остался еще 1 день демо
	static let renewTrialLeft1 = "renew_trial_left_1"

	/// Осталось еще 2 дня демо
	static let renewTrialLeft2 = "renew_trial_left_2"

	/// Осталось еще 3 дня демо
	static let renewTrialLeft3 = "renew_trial_left_3"

	/// Экономия
	static let renewSavings = "renew_savings"

	/// Подписка
	static let renewTitle = "renew_title"

	/// Цена
	static let renewPrice = "renew_price"

	/// Длительность
	static let renewExpiration = "renew_expiration"

	/// Месячная подписка
	static let renewMonthTitle = "renew_month_title"

	/// Годовая подписка
	static let renewYearTitle = "renew_year_title"

	/// Вечная подписка
	static let renewForeverTitle = "renew_forever_title"

	/// 1
	static let renewYearExpiration = "renew_year_expiration"

	/// Год
	static let renewYearExpirationDetail = "renew_year_expiration_detail"

	/// ₽2,72
	static let renewYearPrice = "renew_year_price"

	/// В день
	static let renewYearPriceDetail = "renew_year_price_detail"

	/// 1389
	static let renewYearSaving = "renew_year_saving"

	/// RUB
	static let renewYearSavingDetail = "renew_year_saving_detail"

	/// 😎
	static let renewForeverExpiration = "renew_forever_expiration"

	/// Навсегда
	static let renewForeverExpirationDetail = "renew_forever_expiration_detail"

	/// ₽1490
	static let renewForeverPrice = "renew_forever_price"

	/// RUB
	static let renewForeverPriceDetail = "renew_forever_price_detail"

	/// 1
	static let renewMonthExpiration = "renew_month_expiration"

	/// Месяц
	static let renewMonthExpirationDetail = "renew_month_expiration_detail"

	/// ₽6,41
	static let renewMonthPrice = "renew_month_price"

	/// В день
	static let renewMonthPriceDetail = "renew_month_price_detail"

	/// Чем ты интересуешься?
	static let selectCategoriesTitle = "select_categories_title"

	/// Рекомендуемые
	static let selectCategoriesRecomended = "select_categories_recomended"

	/// Все категории
	static let selectCategoriesAllCategories = "select_categories_all_categories"

	/// Необходимо выбрать хотя бы одну категорию
	static let selectCategoriesAlertNotSelected = "select_categories_alert_not_selected"

	/// Далее
	static let selectCategoriesGoOn = "select_categories_go_on"

	/// Привет, что ты хочешь выучить?
	static let launchTitle = "launch_title"

	/// Примеры
	static let launchExamples = "launch_examples"

	/// Произошла ошибка при попытке загрузки языков. Попробуйте позже или обратитесь в службу тех. поддержки
	static let launchErrorAlertLoadLanguages = "launch_error_alert_load_languages"

	/// Выберите язык
	static let selectLanguageTitle = "select_language_title"

	/// Языки
	static let selectLanguageAllLanguages = "select_language_all_languages"

	/// Поиск
	static let selectLanguageSearchPlaceholder = "select_language_search_placeholder"

	/// Привет ,
	static let subscriptionTitle = "subscription_title"

	/// Получи полный доступ к самым необходимым английским слова и фразам по цене одного занятия с преподавателем
	static let subscriptionGetUnlimitedAccess = "subscription_get_unlimited_access"

	/// Наши предложения
	static let subscriptionOurPlans = "subscription_our_plans"

	/// Демо-доступ
	static let subscriptionTryDemo = "subscription_try_demo"

	/// 12 месяцев: 1 урок
	static let subscription1PlanTip = "subscription_1_plan_tip"

	/// ₽ 83.25 / Месяц
	static let subscription1PlanPrice = "subscription_1_plan_price"

	/// 999.0 ₽ ежегодно
	static let subscription1PlanFullPrice = "subscription_1_plan_full_price"

	/// 1 месяц: 1 кофе
	static let subscription2PlanTip = "subscription_2_plan_tip"

	/// ₽ 199.00 / Месяц
	static let subscription2PlanPrice = "subscription_2_plan_price"

	/// 199.00 ₽ ежемесячно
	static let subscription2PlanFullPrice = "subscription_2_plan_full_price"

	/// Пробный доступ
	static let subscription3PlanTip = "subscription_3_plan_tip"

	/// 999.0 ₽ в год
	static let subscription3PlanPrice = "subscription_3_plan_price"

	/// После 3 дней пробного доступа
	static let subscription3PlanFullPrice = "subscription_3_plan_full_price"

	/// Успейте сегодня!
	static let discountTitle = "discount_title"

	/// - 30%! Предложение для вас!
	static let discountSaleOffer = "discount_sale_offer"

	/// 3 дня бесплатно! Далее 699.0 ₽ в год 
	static let discountPrice = "discount_price"

	/// Отменить подписку можно в любой момент
	static let discountCancelAnyTime = "discount_cancel_any_time"

	/// Почему вас не заинтересовало наше предложение?
	static let discountCancelReasonTitle = "discount_cancel_reason_title"

	/// Дайте обратную связь и мы сделаем приложение лучше!
	static let discountCancelReasonMessage = "discount_cancel_reason_message"

	/// Почему вы отменили подписку?
	static let cancellationReasonTitle = "cancellationReason_title"

	/// Дорого
	static let cancellationReasonExpensiveButton = "cancellationReason_expensive_button"

	/// Не полезно
	static let cancellationReasonNotHealthlyButton = "cancellationReason_not_healthly_button"

	/// Другая причина
	static let cancellationReasonAnotherReasonButton = "cancellationReason_another_reason_button"

	/// Спасибо за ваш отзыв! Мы уже работаем над обновлениями
	static let cancellationReasonActionAlertText = "cancellationReason_action_alert_text"

}
