//
//  AppDelegate.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 18.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import ApphudSDK

import Firebase
import FirebaseMessaging

import FBSDKCoreKit
import Flurry_iOS_SDK
import YandexMobileMetrica

import AVFoundation

import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private let watchSessionManager = WCSessionManagerBlocks()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        String.printEmptyLocalizationKeysPath()
//        print(Global.deviceInfo.udid)
        
        setupStatistics(application, didFinishLaunchingWithOptions: launchOptions)
        
        print("[Global] needToSendEvermoreStatus: \(Global.needToSendEvermoreStatus)")
        API.SystemModule.getToken(needLoadLanguages: true) { result, _ in
            
            print(SharedGlobal.token)
            self.watchSessionManager.sendMessage(["newToken": SharedGlobal.token])
        }
        
        if !Global.isOnboardingCompleted {
            let vc = OnboardingController.xibViewController()
            window?.rootViewController = vc
        } else if !Global.isStartStoryboardCompleted {
            let vc = UIStoryboard.storyboars.start.instantiateInitialViewController()
            window?.rootViewController = vc
        }
        
        return true
    }
}

extension AppDelegate {
    func setupStatistics(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        
        // Firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Analytics.setAnalyticsCollectionEnabled(true)
        
        // ApphudSDK
//        Apphud.enableDebugLogs()
        Apphud.start(apiKey: Constants.appHudApiKey)
        Apphud.setDelegate(self)
        
        SharedGlobal.apphudUserID = Apphud.userID()
        
        watchSessionManager.sendMessage(["newApphudUserID": Apphud.userID()])
        
        // MARK: - FaceBook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        Settings.isAutoLogAppEventsEnabled = false
        Settings.isAdvertiserIDCollectionEnabled = false
        
        // MARK: - Yandex AppMetrica
        YMMYandexMetrica.activate(with: YMMYandexMetricaConfiguration.init(apiKey: Constants.yandexAppMetricaApiKey)!)
        
        print("\n ----Start------ \n")
        print(Apphud.userID())
        print("\n ----End------ \n")
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            print("\n ----Start After 3 sec------ \n")
            print(Apphud.userID())
            print("\n ----End After 3 sec------ \n")
        }
        
        // MARK: - Flurry
        let builder = FlurrySessionBuilder.init()
                                            .withAppVersion(UIApplication.shared.bundleVersion)
                                            .withLogLevel(FlurryLogLevelCriticalOnly)
                                            .withCrashReporting(true)
                                            .withSessionContinueSeconds(10)

        Flurry.startSession(Constants.flurryApiKey, with: builder)
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        try? AVAudioSession.sharedInstance().setActive(true)
    }
}

extension AppDelegate: ApphudDelegate {
    
    func apphudDidChangeUserID(_ userID: String) {
        API.ClientModule.saveNewUserID(userID: userID)
        SharedGlobal.apphudUserID = userID
        
        watchSessionManager.sendMessage(["newApphudUserID": userID])
    }
}
