//
//  SystemModule.swift
//  MajorAutoApp
//
//  Created by Pavel Aristov on 22.11.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import Foundation
import ApphudSDK

import FirebaseAnalytics

extension API {
    class SystemModule {
        class func getToken(needLoadLanguages: Bool, completion: ((Client?, Bool) -> Void)? = nil) {
            
            let parameters: [String: Any] = [
                "registrationId": Apphud.userID() //Global.deviceInfo.udid
            ]
            
            print("getToken: ", Apphud.userID())
            
            API.request("auth", parameters: parameters, method: .post, needToken: false) { content in
                
                guard let token = content["token"] as? String else {
                    print("Can't get `token` in \(#function)")
                    completion?(nil, false)
                    return
                }
                
                SharedGlobal.token = token
                sendPushToken()
                
                if needLoadLanguages {
                    API.LanguageModule.getPairs()
                }
                
                guard let clientMap = content["client"] as? [String: Any] else {
                    print("Can't get `clientMap` in \(#function)")
                    completion?(nil, true)
                    return
                }
                
                let client = Client(from: clientMap)
                client?.setAsCurrentClient()
                
                if Global.needToSendEvermoreStatus {
                    API.SystemModule.setEvermoreStatus()
                }
                
                completion?(client, true)
            }
        }
        
        class func sendPushToken(completion: ((Bool) -> Void)? = nil) {
            
            guard !Global.pushToken.isEmpty else {
                print("[Warning] Push token is empty when required")
                return
            }
            
            let parameters: [String: Any] = [
                "pushToken": Global.pushToken
            ]
            
            API.request("clients/set/push", parameters: parameters, method: .post) { content in
                let result = content["result"] as? Bool ?? false
                print("[API] Send push token result: \(result)")
                completion?(result)
            }
        }
        
        class func setEvermoreStatus(completion: ((Bool) -> Void)? = nil) {
            API.request("payments/evermore", method: .post) { content in
                let result = content["result"] as? Bool ?? false
                print("[API] Result for setEvermoreStatus:", result)
                Global.needToSendEvermoreStatus = !result
                completion?(result)
            }
        }
        
        class func setDemoStatus(completion: ((Bool) -> Void)? = nil) {
            API.request("payments/try/demo", method: .post) { content in
                let result = content["result"] as? Bool ?? false
                print("[API] Result for setDemoStatus:", result)
                completion?(result)
            }
        }
        
        class func sendEvent(_ name: String, parameters: [String: Any] = ["name":"PASHA"]) {
            Analytics.logEvent(name, parameters: parameters)
        }
    }
}
