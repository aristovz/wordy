//
//  TrainingModule.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 07.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation

extension API {
    class TrainingModule {
        class func getNew(completion: @escaping (Training?) -> Void) {
            API.request("words/train") { content in
                let training = Training(from: content)
                completion(training)
            }
        }
        
        class func check(originalWord: String, answer: String, completion: @escaping (String?) -> Void) {
                   
            let parameters: [String: Any] = [
                "originalWord": originalWord,
                "answer": answer
            ]

            API.request("words/train/check", parameters: parameters, method: .post) { content in
                let result = content["result"] as? Bool ?? false
                let rightAnswer = content["rightAnswer"] as? String
                completion(result ? nil : rightAnswer)
            }
        }
        
        class func saveToLearn(originalWord: String, completion: @escaping (Bool) -> Void) {
                   
            let parameters: [String: Any] = [
                "originalWord": originalWord
            ]

            API.request("words/train/save", parameters: parameters, method: .post) { content in
                let result = content["result"] as? Bool ?? false
                completion(result)
            }
        }
    }
}
