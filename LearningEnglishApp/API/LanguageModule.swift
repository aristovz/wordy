//
//  LanguageModule.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 03.12.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import Foundation

extension API {
    class LanguageModule {
        class func getPairs(completion: (([LanguagePair]) -> Void)? = nil) {
            API.request("languages/items") { content in
                guard let languagePairsMap = content["data"] as? [[String: Any]] else {
                    print("Can't get `languagePairsMap` in \(#function)")
                    LanguagePair.availablePairs = []
                    completion?([])
                    return
                }
                
                let langugaePairs = languagePairsMap.compactMap { LanguagePair(from: $0) }
                LanguagePair.availablePairs = langugaePairs
                
                completion?(langugaePairs)
            }
        }
        
        class func getCategories(completion: @escaping ([Category]) -> Void) {
            API.request("categories/items") { content in
                guard let categoriesMap = content["data"] as? [[String: Any]] else {
                    print("Can't get `categoriesMap` in \(#function)")
                    completion([])
                    return
                }
                
                let categories = categoriesMap.compactMap { Category(from: $0) }
                completion(categories)
            }
        }
    }
}
