//
//  API.swift
//  MuseumApp
//
//  Created by Pavel Aristov on 13/06/2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    enum Endpoint: String {
        case debug = "https://wordyapp.ru/api/v1/"
    }
    
    static var url: Endpoint {
        #if DEBUG
            return Endpoint.debug
        #else
            return Endpoint.debug
        #endif
    }

    static func request(_ path: String, parameters: [String: Any] = [:], encoding: ParameterEncoding = URLEncoding.default, method: HTTPMethod = .get, needToken: Bool = true, requestEnd: @escaping ([String: Any]) -> ()) {

        var headers = [String: String]()
        
        if needToken {
            guard addToken(headers: &headers) else {
                requestEnd(["error": "Can't set token to request"])
                return
            }
        }
        
//        print("---------\nREQUEST")
//        print(url.rawValue + path, method.rawValue, parameters, headers, separator: "\n")

        Alamofire.request(url.rawValue + path, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
            
//            print("---------\nRESPONSE (\(url.rawValue + path))")
//            print(response.result.value)
            
            requestEnd(getResponse(from: response))
        }
    }
    
    private static func getResponse(from response: DataResponse<Any>) -> [String: Any] {
        
        let code = response.response?.statusCode
        
        if code != 200 {
            print("[API] Bad status code: \(code ?? -1)")
        }
        
        guard let content = response.result.value as? [String: Any] else {
            print("[API] Can't get `content` from response")
            return ["error": "Can't get `content` from response"]
        }
        
        let success = content["success"] as? Bool
        if let success = success {
            if !success {
                print("[API] Bad `success` status")
            }
        } else {
            print("[API] Can't get `success` status")
        }
        
        guard let data = content["data"] as? [String: Any] else {
            
            guard success ?? false else {
                print("[API] Can't get `data` from `content`")
                return ["error": "Can't get `data` from `content`"]
            }
            
            return ["result": true]
        }
        
        if let error = data["error"] as? String {
            print("[API] Response error: \(error)")
            return ["error": "Response error: \(error)"]
        }
        
        return data
    }

    private static func addToken(headers: inout [String: String]) -> Bool {
        
        guard SharedGlobal.isAuthorized else {
            print("[API] Token is empty when reqiered")
            return false
        }

        headers["X-Auth-Token"] = SharedGlobal.token
        
        return true
    }
}
