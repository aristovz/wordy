//
//  ClientModule.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 03.12.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import Alamofire
import Foundation

extension API {
    class ClientModule {
        class func saveSettings(_ settings: ClientSettings, completion: ((Bool) -> Void)? = nil) {
            
            let parameters: [String: Any] = [
                "client": settings.serverSaveMap
            ]
 
            print(settings.serverSaveMap)
            
            API.request("clients/save", parameters: parameters, encoding: JSONEncoding.default, method: .post) { content in
                
                 guard let clientMap = content["client"] as? [String: Any] else {
                       print("Can't get `clientMap` in \(#function)")
                       completion?(false)
                       return
                   }
                   
                   let client = Client(from: clientMap)
                   client?.setAsCurrentClient()
                
                   completion?(true)
            }
        }
        
        class func saveNewUserID(userID: String, completion: ((Bool) -> Void)? = nil) {
                   
            let parameters: [String: Any] = [
                "registrationId": userID
            ]

            print("[API] Save new userID: \(userID)")
            API.request("clients/set/registration_id", parameters: parameters, encoding: JSONEncoding.default, method: .post) { content in
                let result = content["result"] as? Bool ?? false
                print("[API] Save userID result: \(result)")
                completion?(result)
            }
        }
    }
}
