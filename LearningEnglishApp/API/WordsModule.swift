//
//  WordsModule.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 07.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation

extension API {
    class WordsModule {
        
        class func getToLearn(completion: @escaping (ClientWord?) -> Void) {
            API.request("words/item") { content in
                
                guard let clientWordMap = content["clientword"] as? [String: Any] else {
                    print("Can't get clientWordMap in \(#function)")
                    completion(nil)
                    return
                }
                
                let clientWord = ClientWord(from: clientWordMap)
                completion(clientWord)
            }
        }
        
        class func alreadyKnow(clientwordId: Int, completion: @escaping (ClientWord?) -> Void) {
                   
            let parameters: [String: Any] = [
                "clientwordId": clientwordId
            ]

            API.request("words/learn/know", parameters: parameters, method: .post) { content in
                
                guard let clientWordMap = content["clientword"] as? [String: Any] else {
                    print("Can't get ClientWordMap")
                    completion(nil)
                    return
                }
                
                let clientWord = ClientWord(from: clientWordMap)
                completion(clientWord)
            }
        }
        
        class func repeatWord(clientwordId: Int, completion: @escaping (ClientWord?) -> Void) {
                   
            let parameters: [String: Any] = [
                "clientwordId": clientwordId
            ]

            API.request("words/learn/repeat", parameters: parameters, method: .post) { content in
                
                guard let clientWordMap = content["clientword"] as? [String: Any] else {
                    print("Can't get ClientWordMap")
                    completion(nil)
                    return
                }
                
                let clientWord = ClientWord(from: clientWordMap)
                completion(clientWord)
            }
        }
        
        class func getHistory(completion: @escaping ([ClientWord]) -> Void) {
            
            let parameters: [String: Any] = [
                "limit": 15,
                "offset": 0
            ]
            
            API.request("words/items", parameters: parameters) { content in
                
                guard let wordsMap = content["data"] as? [[String: Any]] else {
                    print("Can't get `wordsMap` in \(#function)")
                    completion([])
                    return
                }
                
                let words = wordsMap.compactMap { ClientWord(from: $0) }
                completion(words)
            }
        }
    }
}
