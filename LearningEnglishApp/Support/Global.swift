//
//  Global.swift
//  Wallpapers
//
//  Created by Pavel Aristov on 07.12.18.
//  Copyright © 2018 aristovz. All rights reserved.
//

import UIKit
import ApphudSDK

class Global {
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate

    static let appID = "1483436808"
    static let appLink = "https://apps.apple.com/ru/app/id\(appID)"
    static let writeReviewLink = "https://itunes.apple.com/app/id\(appID)?action=write-review"
    
    static let supportEmail = "support.products@hotmail.com"
    
    static let termsOfUseLink = "https://vk.com/topic-47585167_43827160"
    static let privacyPolicyLink = "https://vk.com/topic-47585167_41856680"
    
    static func loadOnboarding(in parentViewController: UIViewController) {
        guard let vc = UIStoryboard.storyboars.start.instantiateInitialViewController() else { return }
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        parentViewController.present(vc, animated: true, completion: nil)
    }
    
    static func startDemoPeriod() {
        
        print("[Global] Start demo period")
        
        guard Global.demoPeriodEnded == nil else { return }
        
        API.SystemModule.sendEvent("demo_period_started")
        API.SystemModule.setDemoStatus()
        
        #if DEBUG//targetEnvironment(simulator)
        Global.demoPeriodEnded = Calendar.current.date(byAdding: .second, value: 20, to: Date())
        #else
        Global.demoPeriodEnded = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        #endif
        
        NotificationsService.shared.checkAuthorizationStatus { result in
            guard result else {
                Global.needToAddDemoAlert = true
                return
            }
            
            var timeInterval = 60 * 60 * 24
            #if DEBUG//targetEnvironment(simulator)
            timeInterval = 20
            #endif
            
            Global.planLocalNotification(identifier: "demoPeriodEndedNotification",
                                            title: Localization.commonDemoPeriodExpiredPushTitle.localized,
                                            body: Localization.commonDemoPeriodExpiredPushText.localized,
                                            timeInterval: TimeInterval(timeInterval))
        }
    }
    
    static func startMonthlyDemoPeriod() {
        
        print("[Global] Start monthly demo period")
        
        guard let subs = Apphud.subscription(), subs.productId == Apphud.Product.monthly.rawValue, subs.status == .regular else {
            print("You haven't monthly subscription")
            return
        }
        
        guard Global.monthlyDemoPeriodEnded == nil else { return }
        
        #if DEBUG//targetEnvironment(simulator)
        Global.monthlyDemoPeriodEnded = Calendar.current.date(byAdding: .minute, value: 3, to: Date())
        #else
        Global.monthlyDemoPeriodEnded = Calendar.current.date(byAdding: .day, value: 14, to: Date())
        #endif
        
        var timeInterval = 60 * 60 * 24 * 14
        #if DEBUG //targetEnvironment(simulator)
        timeInterval = 60 * 3
        #endif
        
        planLocalNotification(identifier: "monthlyDemoPeriodEndedNotification",
                              title: Localization.commonMonthlyDemoPeriodExpiredPushTitle.localized,
                              body: Localization.commonMonthlyDemoPeriodExpiredPushText.localized,
                              timeInterval: TimeInterval(timeInterval))
    }
    
    static func planLocalNotification(identifier: String, title: String? = nil, body: String? = nil, userInfo: [AnyHashable: Any] = [:], timeInterval: TimeInterval, repeats: Bool = false) {
        
        let userNotificationCenter = UNUserNotificationCenter.current()
        
        // Create new notifcation content instance
        let notificationContent = UNMutableNotificationContent()

        // Add the content to the notification content
        notificationContent.title = title ?? Localization.commonMonthlyDemoPeriodExpiredPushTitle.localized
        notificationContent.body = body ?? Localization.commonMonthlyDemoPeriodExpiredPushText.localized
        notificationContent.sound = .default
        
        notificationContent.userInfo = userInfo
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: repeats)
        let request = UNNotificationRequest(identifier: identifier, content: notificationContent, trigger: trigger)
        
        userNotificationCenter.add(request) { (error) in
            if let error = error {
                print("[LocalNotification][\(identifier)] Local Notification Error: ", error)
            } else {
                print("[LocalNotification][\(identifier)] Local Notification Added to Queue")
            }
        }
    }
}

extension Global {
    
    static var isOnboardingCompleted: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isOnboardngCompleted") as? Bool ?? false
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "isOnboardngCompleted")
        }
    }
    
    static var isStartStoryboardCompleted: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isStartStoryboardCompleted") as? Bool ?? false
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "isStartStoryboardCompleted")
        }
    }
    
    static var isPushNotificationsEnabled: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isPushNotificationsEnabled") as? Bool ?? false
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "isPushNotificationsEnabled")
        }
    }
    
    static var pushToken: String {
        get {
            return UserDefaults.standard.value(forKey: "pushToken") as? String ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "pushToken")
        }
    }
    
    static var foreverAccess: Bool {
        get {
            return UserDefaults.standard.value(forKey: "foreverAccess") as? Bool ?? false
        }
        set {
            if newValue { API.SystemModule.setEvermoreStatus() }
            UserDefaults.standard.setValue(newValue, forKey: "foreverAccess")
        }
    }
    
    static var needToSendEvermoreStatus: Bool {
        get {
            return UserDefaults.standard.value(forKey: "needToSendEvermoreStatus") as? Bool ?? false
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "needToSendEvermoreStatus")
        }
    }
    
    static var isDemoPeriodEnded: Bool {
        get {
            guard let demoPeriod = demoPeriodEnded else { return false }
            return demoPeriod < Date()
        }
    }
    
    static var demoPeriodEnded: Date? {
        get {
            return UserDefaults.standard.value(forKey: "demoPeriodEnded") as? Date
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "demoPeriodEnded")
        }
    }
    
    static var isMonthlyDemoPeriodEnded: Bool {
        get {
            guard let monthlyDemoPeriodEnded = monthlyDemoPeriodEnded else { return false }
            return monthlyDemoPeriodEnded < Date()
        }
    }
    
    static var monthlyDemoPeriodEnded: Date? {
        get {
            return UserDefaults.standard.value(forKey: "monthlyDemoPeriodEnded") as? Date
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "monthlyDemoPeriodEnded")
        }
    }
    
    static var needToAddDemoAlert: Bool {
        get {
            return UserDefaults.standard.value(forKey: "needToAddDemoAlert") as? Bool ?? false
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "needToAddDemoAlert")
        }
    }
}
