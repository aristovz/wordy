//
//  SharedGlobal.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 03.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation

class SharedGlobal {
    static var isAuthorized: Bool {
        return !SharedGlobal.token.isEmpty
    }
    
    static var token: String {
        get {
//            print(UserDefaults.standard.string(forKey: "token") ?? "")
            return UserDefaults.standard.string(forKey: "token") ?? ""
        }
        set {
            print("[SharedGlobal] Token is set\n", newValue)
            UserDefaults.standard.set(newValue, forKey: "token")
        }
    }
    
    static var apphudUserID: String {
        get {
//            print(UserDefaults.standard.string(forKey: "apphudUserID") ?? "")
            return UserDefaults.standard.string(forKey: "apphudUserID") ?? ""
        }
        set {
            print("[SharedGlobal] ApphudUserID is set\n", newValue)
            UserDefaults.standard.set(newValue, forKey: "apphudUserID")
        }
    }
}
