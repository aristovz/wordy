//
//  Pallete.swift
//  Wallpapers
//
//  Created by Pavel Aristov on 06.12.18.
//  Copyright © 2018 aristovz. All rights reserved.
//

import UIKit

class Pallete {
    
    static let buttonGradientStart = UIColor(hexString: "57C1C8")
    static let buttonGradientEnd = UIColor(hexString: "00C0CD")
    static let buttonShadowColor = UIColor(hexString: "8258E1").withAlphaComponent(0.47)
    
    static let blueGradient = UIColor(hexString: "2FC0CA")
    
    static let purple = UIColor(hexString: "6226FF")
    static let shadow = UIColor(hexString: "D2CFFF")
    
    struct LanguageCell {
        static let purple = UIColor(hexString: "6226FF")
        static let gray = UIColor(hexString: "404040")
    }
    
    struct SelectLanguageController {
        static let shadow = UIColor(hexString: "0A93F2")
    }
    
    struct WordsController {
        static let shareBorder = UIColor(hexString: "EDEDED")
    }
    
    struct SettingsController {
        static let stackViewButtonsNormalText = UIColor(hexString: "8F99B4")
        static let stackViewButtonsSelectedText = UIColor.white
        
        static let stackViewButtonsNormalBackground = UIColor.clear
        static let stackViewButtonsSelectedBackground = Pallete.blueGradient
    }
    
    struct TrainingController {
        static let red = UIColor(hexString: "FC0042")
        static let purple = Pallete.purple
    }
    
    struct RenewTrialController {
        static let shadow = UIColor(hexString: "D2CFFF")
    }
}

extension Pallete {
    
    private static let wordSampleColorsHex = [
        "29D396", // 1
        "FFB82C", // 2
        "DF77FF", // 3
        "FF7474", // 4
        "0093F2"  // 5
    ]
    
    static func wordSampleColor(for position: Int) -> UIColor {
        
        let colors = Pallete.wordSampleColorsHex
        
        guard Array(0..<colors.count).contains(position) else {
            let safetyPosition = max(0, min(position, colors.count - 1))
            return UIColor(hexString: colors[safetyPosition])
        }

        return UIColor(hexString: colors[position])
    }
}
