//
//  Extension_UIFont.swift
//  FitnesApp
//
//  Created by Pavel Aristov on 18.01.19.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit

extension UIFont {
    
    enum Futura: String {
        case book = "FuturaBT-Book"
        case medium = "Futura-Medium"
        case mediumItalic = "Futura-MediumItalic"
        case condensedMedium = "Futura-CondensedMedium"
        
        case bold = "Futura-Bold"
        case condensedExtraBold = "Futura-CondensedExtraBold"
    }
    
    enum LucidaGrande: String {
        case regular = "LucidaGrande"
        case bold = "LucidaGrande-Bold"
    }
    
    enum Avenir: String {
        case roman = "Avenir-Roman"
        case medium = "Avenir-Heavy"
    }
    
    class func getAvenirFont(font: Avenir = .medium, size: CGFloat = 18) -> UIFont {
        return UIFont(name: font.rawValue, size: size)!
    }
    
//    class func getFuturaFont(font: Futura = .medium, size: CGFloat = 18) -> UIFont {
//        return UIFont(name: font.rawValue, size: size)!
//    }
    
    class func getLucidaGrandeFont(font: LucidaGrande = .regular, size: CGFloat = 18) -> UIFont {
        return UIFont(name: font.rawValue, size: size)!
    }
    
    class func printFonts() {
        UIFont.familyNames.forEach { family in
            print("\(family): \(UIFont.fontNames(forFamilyName: family))")
        }
    }
}
