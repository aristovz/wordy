//
//  Extensions_UIView.swift
//  HoroscopeApp
//
//  Created by Pavel Aristov on 06.12.18.
//  Copyright © 2018 aristovz. All rights reserved.
//

import UIKit

extension UIViewController {
    var statusBar: UIView {
        return UIApplication.shared.value(forKey: "statusBar") as! UIView
    }
    
    func updateGradients(gradients: [UIView: CALayer]) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        gradients.forEach({ $0.value.frame = $0.key.bounds })
        CATransaction.commit()
    }
}

extension UIView {
    
    func show(_ animated: Bool = true, completion: (() -> Void)? = nil) {
        let duration = animated ? 0.15 : 0
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
        }, completion: { _ in
            completion?()
        })
    }
    
    func hide(_ animated: Bool = true, completion: (() -> Void)? = nil) {
        let duration = animated ? 0.15 : 0
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0
        }, completion: { _ in
            completion?()
        })
    }
    
    func fadeTransition(_ duration:CFTimeInterval) {
        layer.removeAnimation(forKey: CATransitionType.fade.rawValue)
        
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func shakeZ(duration: CFTimeInterval) {
        let translation = CAKeyframeAnimation(keyPath: "transform.translation.x");
        translation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        translation.values = [-5, 5, -2, 2, 0]
        
        let rotation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        rotation.values = [-5, 5, -2, 2, 0].map {
            (degrees: Double) -> Double in
            let radians: Double = (Double.pi * degrees) / 180.0
            return radians
        }
        
        let shakeGroup: CAAnimationGroup = CAAnimationGroup()
        shakeGroup.animations = [translation, rotation]
        shakeGroup.duration = duration
        self.layer.add(shakeGroup, forKey: "shakeIt")
    }
    
    // { x: 0, y: 0 |- }  { x: 1, y: 1 _| }
    @discardableResult
    func setGradient(name: String, startPoint: CGPoint = CGPoint(x: 0, y: 0.0), endPoint: CGPoint = CGPoint(x: 1, y: 1), colors: [UIColor]) -> CAGradientLayer {
        
        removeGradient(with: name)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.name = name
        
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
        gradientLayer.colors = colors.map({ $0.cgColor })
        gradientLayer.frame = self.bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
        
        return gradientLayer
    }
    
    func setButtonGradient() -> CAGradientLayer {
        return self.setGradient(name: "buttonGradient", startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0), colors: [Pallete.buttonGradientStart, Pallete.buttonGradientEnd])
    }
    
    func removeGradient(with name: String) {
        self.layer.sublayers?.filter({ $0.name == name }).forEach({ $0.removeFromSuperlayer() })
    }
    
    func setDefaultShadow() {
        self.dropShadow(color: Pallete.shadow, opacity: 0.25, offSet: CGSize(width: 0, height: 30), radius: 50)
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath//UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func dropInnerShadow(to edges: [UIRectEdge], opacity: Float = 0.5, radius: CGFloat = 3.0, offset: CGFloat = 1.0, color: UIColor = UIColor.black) {
        
        self.layer.sublayers?.filter({ $0.name == "innerShadow" }).forEach({ $0.removeFromSuperlayer() })
        
        let size = self.frame.size
        self.clipsToBounds = true
        
        for edge in edges {
            
            let layer: CALayer = CALayer()
            layer.name = "innerShadow"
            layer.backgroundColor = UIColor.white.cgColor
            
            switch edge {
            case .top:
                layer.position = CGPoint(x: size.width / 2, y: -size.height / 2 + 0.5)
                layer.shadowOffset = CGSize(width: offset, height: offset)
            case .bottom:
                layer.position = CGPoint(x: size.width / 2, y: 3 * size.height / 2 - 0.5)
                layer.shadowOffset = CGSize(width: -offset / 2, height: -offset / 2)
            case .left:
                layer.position = CGPoint(x: -size.width / 2 + 0.5, y: size.height / 2)
                layer.shadowOffset = CGSize(width: offset, height: offset)
            case .right:
                layer.position = CGPoint(x: 3 * size.width / 2 - 0.5, y: size.height / 2)
                layer.shadowOffset = CGSize(width: -offset, height: -offset)
            default:
                break
            }
            
            layer.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            layer.shadowColor = color.cgColor
            
            layer.shadowOpacity = opacity
            layer.shadowRadius = radius

            self.layer.addSublayer(layer)
        }
    }

    func removeAllShadows() {
        if let sublayers = self.layer.sublayers, !sublayers.isEmpty {
            for sublayer in sublayers {
                sublayer.removeFromSuperlayer()
            }
        }
    }
    
    @discardableResult func addBlurEffect(style: UIBlurEffect.Style = .dark, alpha: CGFloat = 1, belowSubview: UIView? = nil) -> UIVisualEffectView {
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = alpha
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if let bellowSubview = belowSubview {
            self.insertSubview(blurEffectView, belowSubview: bellowSubview)
        } else { self.addSubview(blurEffectView) }
        
        return blurEffectView
    }
    
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        self.layer.roundCorners(corners, radius: radius)
    }

}

extension CALayer {
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        if #available(iOS 11, *) {
            self.cornerRadius = radius
            self.maskedCorners = corners
        } else {
            var cornerMask = UIRectCorner()
            if(corners.contains(.layerMinXMinYCorner)){
                cornerMask.insert(.topLeft)
            }
            if(corners.contains(.layerMaxXMinYCorner)){
                cornerMask.insert(.topRight)
            }
            if(corners.contains(.layerMinXMaxYCorner)){
                cornerMask.insert(.bottomLeft)
            }
            if(corners.contains(.layerMaxXMaxYCorner)){
                cornerMask.insert(.bottomRight)
            }
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.mask = mask
        }
    }
}
