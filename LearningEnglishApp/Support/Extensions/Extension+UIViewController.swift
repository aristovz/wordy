//
//  Extension+UIViewController.swift
//  Fitness
//
//  Created by Pavel Aristov on 27/05/2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import MessageUI

extension UIViewController {
    //error?.localizedDescription
    func showAlert(text: String, title: String? = Localization.commonWarning.getLzText(original: "Внимание"), titleAction: String = Localization.commonOk.getLzText(original: "ОK"), action: (() -> ())? = nil) {
        let alertWarning = UIAlertController(title: title, message: text, preferredStyle: .alert)
        
        let dismisAction = UIAlertAction(title: titleAction, style: .default) {
            (alert: UIAlertAction!) -> Void in
            action?()
        }
        
        alertWarning.addAction(dismisAction)
        present(alertWarning, animated: true, completion: nil)
    }
    
    func showWarningAlert(text: (key: String, original: String), title: (key: String, original: String) = (Localization.commonWarning, "Внимание"), titleAction: String = Localization.commonOk.getLzText(original: "ОK"), action: (() -> ())? = nil) {
        
        let _title = title.key.getLzText(original: title.original)
        let _text = text.key.getLzText(original: text.original)
        
        showAlert(text: _text, title: _title, titleAction: titleAction, action: action)
    }
    
    func hideKeyboardWhenTappedAround(cancelsTouchesInView: Bool = true) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = cancelsTouchesInView
        view.addGestureRecognizer(tap)
    }
    
    func addDoneButtonToTextInputsViews(views: [UIView], toolBarStyle: UIBarStyle = UIBarStyle.default, buttonColor: UIColor = .black) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.barStyle = toolBarStyle
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(UIViewController.dismissKeyboard))
        doneButton.tintColor = buttonColor
        
        toolBar.setItems([flexSpace, doneButton], animated: false)
        
        _ = views.map {
            if let textView = $0 as? UITextView {
                textView.inputAccessoryView = toolBar
            }
            else if let textField = $0 as? UITextField {
                textField.inputAccessoryView = toolBar
            }
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setStatusBarStyle(_ style: UIStatusBarStyle) {
        if #available(iOS 13.0, *) {
//            let navBarAppearance = UINavigationBarAppearance()
//            navBarAppearance.configureWithOpaqueBackground()
//            navBarAppearance.backgroundColor = style == .lightContent ? UIColor.white : .black
//            UINavigationBar.appearance(whenContainedInInstancesOf: [UINavigationController.self]).standardAppearance = navBarAppearance
//            UINavigationBar.appearance(whenContainedInInstancesOf: [UINavigationController.self]).scrollEdgeAppearance = navBarAppearance
        } else if let statusBar = UIApplication.shared.statusBarUIView {
            statusBar.setValue(style == .lightContent ? UIColor.white : .black, forKey: "foregroundColor")
        }
    }
    
    func presentFull(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)?) {
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        self.present(viewControllerToPresent, animated: animated, completion: completion)
    }
}

extension UIViewController: MFMailComposeViewControllerDelegate {
    
    @discardableResult
    func openURL(string: String) -> Bool {
        guard let url = URL(string: string) else { return false }
        guard UIApplication.shared.canOpenURL(url) else { return false }
        
        UIApplication.shared.open(url)
        return true
    }
    
    @discardableResult
    func openMailController(_ recipients: [String] = []) -> Bool {
        
        guard MFMailComposeViewController.canSendMail() else { return false }
        
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients(recipients)
        
        mail.setSubject("Wordy")
        
        let body = """

        
        Information for developers
        ApphudID: \(SharedGlobal.apphudUserID)
        """
        
        mail.setMessageBody(body, isHTML: false)
        
        present(mail, animated: true)
        
        return true
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true)
    }
}

extension UIApplication {
    var statusBarUIView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 3848
            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = tag
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        } else {
            return nil
        }
    }
}

extension UIApplication {
    /// The top most view controller
    static var topMostViewController: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController?.visibleViewController
    }
}

extension UIViewController {
    /// The visible view controller from a given view controller
    var visibleViewController: UIViewController? {
        if let navigationController = self as? UINavigationController {
            return navigationController.topViewController?.visibleViewController
        } else if let tabBarController = self as? UITabBarController {
            return tabBarController.selectedViewController?.visibleViewController
        } else if let presentedViewController = presentedViewController {
            return presentedViewController.visibleViewController
        } else {
            return self
        }
    }
}
