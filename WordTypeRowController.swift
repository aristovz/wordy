//
//  WordTypeRowController.swift
//  WordyWatchApp Extension
//
//  Created by Pavel Aristov on 03.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import WatchKit
import Foundation

class WordTypeRowController: NSObject {
    
    @IBOutlet private weak var textLabel: WKInterfaceLabel!
    @IBOutlet private weak var translateLabel: WKInterfaceLabel!
    
    @IBOutlet private weak var separator: WKInterfaceSeparator!
    
    func setupUI(_ word: Word, isLastSample: Bool) {
        textLabel.setText(word.translationWord)
        translateLabel.setText(word.originalWord)
        
        separator.setHidden(!isLastSample)
    }
}
