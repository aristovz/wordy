//
//  InterfaceController.swift
//  WordyWatchApp Extension
//
//  Created by Pavel Aristov on 02.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

import Foundation
import AVFoundation

import WatchKit
import WatchConnectivity

struct WordS {
    let text: String
    let translate: String
}

class InterfaceController: WKInterfaceController {

    @IBOutlet private weak var tableView: WKInterfaceTable!
    
    private var watchSessionManager = WCSessionManagerBlocks()
    
    private let speechSynthesizer = AVSpeechSynthesizer()
    
    private var currentWord: ClientWord? = nil {
        didSet {
            setupTableData()
        }
    }
    
    private var historyWords: [ClientWord] = [] {
        didSet {
            setupTableData()
        }
    }
    
    private var currentWordWithSamples: [Word] {
        
        guard let currentWord = currentWord else {
            return []
        }
        
        return [currentWord.word] + currentWord.word.samples.filter { $0.id != -1 }
    }
    
    private var historyWordsWithSamples: [Word] {
        return historyWords.flatMap { [$0.word] + $0.word.samples }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        setupTableData()
        
        watchSessionManager.didReceiveMessage = { message in
            self.parseRecievedMessages(message)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        refreshData()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func setupTableData() {
        
        tableView.removeRows(at: IndexSet(integersIn: 0...tableView.numberOfRows))
        
        tableView.insertRows(at: IndexSet(integer: 0), withRowType: "sectionTitleType")
        
        if let titleRowController = tableView.rowController(at: 0) as? SectionTitleTypeRowController {
            titleRowController.setupUI(title: NSLocalizedString("watch_learning_section_title", comment: "Изучаемое слово"))
        }
        
        if let currentWord = currentWord {
            addRowsForCientWord(currentWord, isInHistory: false)
        }
        
        tableView.insertRows(at: IndexSet(integer: tableView.numberOfRows), withRowType: "sectionTitleType")
        
        if let titleRowController = tableView.rowController(at: tableView.numberOfRows - 1) as? SectionTitleTypeRowController {
            titleRowController.setupUI(title: NSLocalizedString("watch_history_section_title", comment: "Изученные слова"))
        }
        
        for k in 0..<historyWords.count {
            addRowsForCientWord(historyWords[k], isInHistory: k != historyWords.count - 1)
        }
    }
    
    private func addRowsForCientWord(_ clientWord: ClientWord, isInHistory: Bool) {
        
        let numbersOfRows = tableView.numberOfRows
        
        tableView.insertRows(at: IndexSet(integer: numbersOfRows), withRowType: "wordRowType")
        
        if let wordController = tableView.rowController(at: numbersOfRows) as? WordTypeRowController {
            wordController.setupUI(clientWord.word, isLastSample: clientWord.word.samples.isEmpty)
        }
        
        for k in 0..<clientWord.word.samples.count {
            
            let word = clientWord.word.samples[k]
            
            guard word.id != -1 else { continue }
            
            tableView.insertRows(at: IndexSet(integer: k + numbersOfRows + 1), withRowType: "wordRowType")
            
            guard let rowController = tableView.rowController(at: k + numbersOfRows + 1) as? WordTypeRowController else { continue }
            
            let isLastSample = isInHistory ? k == (clientWord.word.samples.count - 1) : false
            rowController.setupUI(word, isLastSample: isLastSample)
        }
    }
    
    private func refreshData() {
        API.WordsModule.getToLearn { clientWord in
            guard let clientWord = clientWord else {
                print("Can't get `clientWord` in \(#function)")
                return
            }
            
            self.currentWord = clientWord
        }
        
        API.WordsModule.getHistory { words in
            guard !words.isEmpty else { return }
            self.historyWords = words
        }
    }
}

extension InterfaceController {
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        speechSynthesizer.stopSpeaking(at: .immediate)
        
        var string = ""
        
        if let _ = currentWord, rowIndex - 1 < currentWordWithSamples.count {
            string = currentWordWithSamples[rowIndex - 1].translationWord
        } else {
            string = historyWordsWithSamples[rowIndex - currentWordWithSamples.count - 2].translationWord
        }
        
        
        let utterance = AVSpeechUtterance(string: string)
        utterance.voice = AVSpeechSynthesisVoice(language: "en")
        utterance.rate = 0.4

        speechSynthesizer.speak(utterance)
    }
}

extension InterfaceController {
    private func parseRecievedMessages(_ message: [String: Any]) {
        if let newCurrentWordMap = message["newCurrentWord"] as? [String: Any],
            let newCurrentWord = ClientWord(from: newCurrentWordMap) {
            print("[WatchApp Extension] Update `currentWord`:", newCurrentWord.word.originalWord)
            guard currentWord?.id != newCurrentWord.id else { return }
            currentWord = newCurrentWord
        } else if let newHistoryMap = message["newHistory"] as? [[String: Any]] {
            print("[WatchApp Extension] Update `history`:", newHistoryMap.count)
            historyWords = newHistoryMap.compactMap { ClientWord(from: $0) }
        } else if let newToken = message["newToken"] as? String {
            print("[WatchApp Extension] Update `token`:", newToken)
            SharedGlobal.token = newToken
            refreshData()
        } else if let newApphudUserID = message["newApphudUserID"] as? String {
            print("[WatchApp Extension] Update `newApphudUserID`:", newApphudUserID)
            SharedGlobal.apphudUserID = newApphudUserID
        }
    }
}
