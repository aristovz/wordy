//
//  SectionTitleTypeRowController.swift
//  WordyWatchApp Extension
//
//  Created by Pavel Aristov on 03.03.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import WatchKit
import Foundation

class SectionTitleTypeRowController: NSObject {
    
    @IBOutlet private weak var titleLabel: WKInterfaceLabel!
    
    func setupUI(title: String) {
        self.titleLabel.setText(title)
    }
}
